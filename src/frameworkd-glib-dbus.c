/*
 *  Copyright (C) 2008
 *      Authors (alphabetical) :
 *              Marc-Olivier Barre <marco@marcochapeau.org>
 *              Julien Cassignol <ainulindale@gmail.com>
 *              Klaus 'mrmoku' Kurzamnn <mok@fluxnetz.de>
 *              quickdev
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "ogsmd/frameworkd-glib-ogsmd-dbus.h"
#include "ogsmd/frameworkd-glib-ogsmd-call.h"
#include "ogsmd/frameworkd-glib-ogsmd-sim.h"
#include "ogsmd/frameworkd-glib-ogsmd-network.h"
#include "ogsmd/frameworkd-glib-ogsmd-device.h"
#include "ogsmd/frameworkd-glib-ogsmd-sms.h"
#include "ogsmd/frameworkd-glib-ogsmd-pdp.h"
#include "odeviced/frameworkd-glib-odeviced-dbus.h"
#include "odeviced/frameworkd-glib-odeviced-idlenotifier.h"
#include "odeviced/frameworkd-glib-odeviced-powersupply.h"
#include "odeviced/frameworkd-glib-odeviced-realtimeclock.h"
#include "odeviced/frameworkd-glib-odeviced-input.h"
#include "ousaged/frameworkd-glib-ousaged-dbus.h"
#include "ousaged/frameworkd-glib-ousaged.h"
#include "opimd/frameworkd-glib-opimd-dbus.h"
#include "opimd/frameworkd-glib-opimd-messages.h"
#include "opimd/frameworkd-glib-opimd-contacts.h"
#include "opimd/frameworkd-glib-opimd-calls.h"
#include "opimd/frameworkd-glib-opimd-tasks.h"
#include "opreferencesd/frameworkd-glib-opreferencesd-dbus.h"
#include "opreferencesd/frameworkd-glib-opreferencesd-preferences.h"

#include "dialer-marshal.h"

DBusGConnection *bus;

void
lose(const char *str, ...)
{
	va_list args;

	va_start(args, str);

	vfprintf(stderr, str, args);
	fputc('\n', stderr);

	va_end(args);

	// calling exit in a lib is nasty... not?
	//exit (1);
}

void
lose_gerror(const char *prefix, GError * error)
{
	lose("%s: %s, %d (%s), code %d", prefix, error->message, error->domain,
	     g_quark_to_string(error->domain), error->code);
}


GError *
dbus_handle_errors(GError * dbus_error)
{
	const char *error_name;
	GError *error = NULL;
	if (dbus_error->domain == DBUS_GERROR) {
		if (dbus_error->code == DBUS_GERROR_REMOTE_EXCEPTION) {
			error_name = dbus_g_error_get_name(dbus_error);

			if (!strncmp
			    (error_name, SIM_INTERFACE,
			     strlen(SIM_INTERFACE))) {
				error = ogsmd_sim_handle_errors(dbus_error);
			}
			else if (!strncmp
				 (error_name, CALL_INTERFACE,
				  strlen(CALL_INTERFACE))) {
				error = ogsmd_call_handle_errors(dbus_error);
			}
			else if (!strncmp
				 (error_name, NETWORK_INTERFACE,
				  strlen(NETWORK_INTERFACE))) {
				error = ogsmd_network_handle_errors(dbus_error);
			}
			else if (!strncmp
				 (error_name, DEVICE_INTERFACE,
				  strlen(DEVICE_INTERFACE))) {
				error = ogsmd_device_handle_errors(dbus_error);
			}
			else if (!strncmp
				 (error_name, USAGE_INTERFACE,
				  strlen(USAGE_INTERFACE))) {
				error = ousaged_handle_errors(dbus_error);
			}
			else if (!strncmp
				 (error_name, RESOURCE_INTERFACE,
				  strlen(RESOURCE_INTERFACE))) {
				error = ousaged_handle_resource_errors
					(dbus_error);
			}
            else if (!strncmp
                 (error_name, DBUS_FSO_ERROR_UNAVAILABLE,
                  strlen(DBUS_FSO_ERROR_UNAVAILABLE))) {
                error = g_error_new(FRAMEWORKD_GLIB_DBUS_ERROR, FRAMEWORKD_GLIB_DBUS_ERROR_SERVICE_NOT_AVAILABLE, "TODO %s",
                    error_name);
            }
			else {
				lose_gerror("Failed to handle dbus error",
					    dbus_error);
			}
		}
		else {
			error = dbus_handle_internal_errors(dbus_error);
		}
	}
	else {
		lose_gerror("Unknown dbus error", dbus_error);
	}

	return error;
}


GError *
dbus_handle_internal_errors(GError * error)
{
	int dbusError = 0;

	if (error->code == DBUS_GERROR_SERVICE_UNKNOWN) {
		dbusError = FRAMEWORKD_GLIB_DBUS_ERROR_SERVICE_NOT_AVAILABLE;
	}
	else if (error->code == DBUS_GERROR_NO_REPLY) {
		dbusError = FRAMEWORKD_GLIB_DBUS_ERROR_NO_REPLY;
	}
	else {
		lose_gerror("Unknown internal dbus error", error);
	}
	//dbus_connect_to_bus(NULL);
	return g_error_new(FRAMEWORKD_GLIB_DBUS_ERROR, dbusError, "%s",
			   error->message);
}

DBusGProxy *
dbus_connect_to_interface(const char *bus_name, const char *path,
			  const char *interface, const char *interface_name)
{
	DBusGProxy *itf = NULL;
	if (bus != NULL) {
		itf = dbus_g_proxy_new_for_name(bus, bus_name, path, interface);
		if (itf == NULL) {
			printf("Couln't connect to the %s Interface",
			       interface_name);
		}
	}
	return itf;
}

void
dbus_free_data(GType type, gpointer data)
{
	GValue foo;
	g_value_init(&foo, type);
	g_value_take_boxed(&foo, data);
	g_value_unset(&foo);
}

GType
dbus_get_type_g_string_variant_hashtable()
{
	static GType foo = 0;
	if (G_UNLIKELY(foo == 0))
		foo = dbus_g_type_get_map("GHashTable", G_TYPE_STRING,
					  G_TYPE_VALUE);
	return foo;
}

GType
dbus_get_type_g_string_int_int_int_array()
{
	static GType foo = 0;
	if (G_UNLIKELY(foo == 0))
		foo = dbus_g_type_get_collection("GPtrArray",
						 dbus_g_type_get_struct
						 ("GValueArray", G_TYPE_STRING,
						  G_TYPE_INT, G_TYPE_INT,
						  G_TYPE_INT, G_TYPE_INVALID));
	return foo;
}

GType
dbus_get_type_int_g_string_g_string_variant_hashtable_array()
{
	static GType foo = 0;
	if (G_UNLIKELY(foo == 0))
		foo = dbus_g_type_get_collection("GPtrArray",
						 dbus_g_type_get_struct
						 ("GValueArray", G_TYPE_INT,
						  G_TYPE_STRING,
						  dbus_g_type_get_map
						  ("GHashTable", G_TYPE_STRING,
						   G_TYPE_VALUE),
						  G_TYPE_INVALID));
	return foo;
}

GType
dbus_get_type_int_g_string_g_string_g_string_array()
{
	static GType foo = 0;
	if (G_UNLIKELY(foo == 0))
		foo = dbus_g_type_get_collection("GPtrArray",
						 dbus_g_type_get_struct
						 ("GValueArray", G_TYPE_INT,
						  G_TYPE_STRING, G_TYPE_STRING,
						  G_TYPE_STRING,
						  G_TYPE_INVALID));
	return foo;
}


FrameworkdHandler *
frameworkd_handler_new()
{
	FrameworkdHandler *h = NULL;
	h = malloc(sizeof(FrameworkdHandler));
	if (h == NULL) {
		g_debug("Couldn't alloc frameworkdHandler.");
		return NULL;
	}

	h->networkStatus = NULL;
	h->networkSignalStrength = NULL;
	h->simAuthStatus = NULL;
	h->simReadyStatus = NULL;
	h->simIncomingStoredMessage = NULL;
    h->gsmDeviceStatus = NULL;   
	h->callCallStatus = NULL;
	h->pdpNetworkStatus = NULL;
	h->pdpContextStatus = NULL;
	h->deviceIdleNotifierState = NULL;
	h->devicePowerSupplyStatus = NULL;
	h->devicePowerSupplyCapacity = NULL;
	h->deviceWakeupTimeChanged = NULL;
	h->deviceInputEvent = NULL;
	h->incomingUssd = NULL;
	h->incomingMessageReceipt = NULL;
	h->usageResourceAvailable = NULL;
	h->usageResourceChanged = NULL;
	h->pimNewMessage = NULL;
	h->pimUpdatedMessage = NULL;
	h->pimDeletedMessage = NULL;
	//h->pimMovedMessage = NULL;
	h->pimIncomingMessage = NULL;
	h->pimUnreadMessages = NULL;
	h->pimNewContact = NULL;
	h->pimUpdatedContact = NULL;
	h->pimDeletedContact = NULL;
	h->pimNewCall = NULL;
	h->pimIncomingCall = NULL;
	h->pimNewMissedCalls = NULL;
	h->pimUnfinishedTasks = NULL;
	h->preferencesNotify = NULL;

	return h;
}



void
frameworkd_handler_connect(FrameworkdHandler * frameworkdHandler)
{
	if (frameworkdHandler != NULL)
		fwdHandlers = frameworkdHandler;

	GError *error = NULL;
// g_type_init will be deprecated in 2.36. 2.35 is the development
// version for 2.36, hence do not call g_type_init starting 2.35.
// http://developer.gnome.org/gobject/unstable/gobject-Type-Information.html#g-type-init
#if !GLIB_CHECK_VERSION(2, 35, 0)
	g_type_init();
#endif

	g_debug("Trying to get the system bus");
	bus = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);


	if (!bus)
		lose_gerror("Couldn't connect to system bus", error);


	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__INT_STRING_BOXED, G_TYPE_NONE,
		 G_TYPE_INT, G_TYPE_STRING,
		 dbus_get_type_g_string_variant_hashtable(), G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__BOXED, G_TYPE_NONE,
		 dbus_get_type_g_string_variant_hashtable(), G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__UINT_BOOLEAN_STRING, G_TYPE_NONE,
		 G_TYPE_UINT, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_INVALID);
	dbus_g_object_register_marshaller(g_cclosure_marshal_VOID__INT,
					  G_TYPE_NONE, G_TYPE_INT,
					  G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_STRING, G_TYPE_NONE,
		 G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_STRING_STRING, G_TYPE_NONE,
		 G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_BOOLEAN_BOXED,
		 G_TYPE_NONE, G_TYPE_STRING, G_TYPE_BOOLEAN,
		 dbus_get_type_g_string_variant_hashtable(), G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_BOOLEAN, G_TYPE_NONE,
		 G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_STRING_INT, G_TYPE_NONE,
		 G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_BOXED, G_TYPE_NONE,
		 G_TYPE_STRING, G_TYPE_BOXED, G_TYPE_INVALID);
	dbus_g_object_register_marshaller
		(g_cclosure_user_marshal_VOID__STRING_STRING_BOXED,
		 G_TYPE_NONE, G_TYPE_STRING, G_TYPE_STRING,
		 dbus_get_type_g_string_variant_hashtable(), G_TYPE_INVALID);

	if (frameworkdHandler != NULL) {
		g_debug("Adding signals.");
		if (frameworkdHandler->networkStatus != NULL) {
			dbus_connect_to_ogsmd_network();
			dbus_g_proxy_add_signal(networkBus, "Status",
						dbus_get_type_g_string_variant_hashtable
						(), G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(networkBus, "Status",
						    G_CALLBACK
						    (ogsmd_network_status_handler),
						    frameworkdHandler->networkStatus,
						    NULL);

			g_debug("registered to GSM.Network.Status");
		}
		if (frameworkdHandler->networkSignalStrength != NULL) {
			dbus_connect_to_ogsmd_network();
			dbus_g_proxy_add_signal(networkBus, "SignalStrength",
						G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(networkBus,
						    "SignalStrength",
						    G_CALLBACK
						    (ogsmd_network_signal_strength_handler),
						    frameworkdHandler->networkSignalStrength,
						    NULL);
			g_debug("registered to GSM.Network.SignalStrength");
		}
		if (frameworkdHandler->simAuthStatus != NULL) {
			dbus_connect_to_ogsmd_sim();
			dbus_g_proxy_add_signal(simBus, "AuthStatus",
						G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(simBus, "AuthStatus",
						    G_CALLBACK
						    (ogsmd_sim_auth_status_handler),
						    frameworkdHandler->simAuthStatus,
						    NULL);
			g_debug("registered to GSM.SIM.AuthStatus");
		}
		if (frameworkdHandler->simReadyStatus != NULL) {
			dbus_connect_to_ogsmd_sim();
			dbus_g_proxy_add_signal(simBus, "ReadyStatus",
						G_TYPE_BOOLEAN, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(simBus, "ReadyStatus",
						    G_CALLBACK
						    (ogsmd_sim_ready_status_handler),
						    frameworkdHandler->simReadyStatus,
						    NULL);
			g_debug("registered to GSM.SIM.ReadyStatus");
		}
		if (frameworkdHandler->simIncomingStoredMessage != NULL) {
			dbus_connect_to_ogsmd_sim();
			dbus_g_proxy_add_signal(simBus, "IncomingStoredMessage",
						G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(simBus,
						    "IncomingStoredMessage",
						    G_CALLBACK
						    (ogsmd_sim_incoming_stored_message_handler),
						    frameworkdHandler->simIncomingStoredMessage,
						    NULL);
			g_debug("registered to GSM.SIM.IncomingStoredMessage");
		}

        if (frameworkdHandler->gsmDeviceStatus != NULL) {
            dbus_connect_to_ogsmd_device();
            dbus_g_proxy_add_signal(deviceBus,
                    "DeviceStatus",
                    G_TYPE_STRING, G_TYPE_INVALID);
            dbus_g_proxy_connect_signal(deviceBus,
                    "DeviceStatus", G_CALLBACK(
                        ogsmd_device_status_handler),
                    frameworkdHandler->gsmDeviceStatus, NULL);
            g_debug("registered to GSM.Device.DeviceStatus");
        }

        // NOTE ported to new signal handling method
		if (frameworkdHandler->callCallStatus != NULL) {
			dbus_connect_to_ogsmd_call();
			dbus_g_proxy_add_signal(callBus, "CallStatus",
						G_TYPE_INT, G_TYPE_STRING,
						dbus_get_type_g_string_variant_hashtable
						(), G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(callBus, "CallStatus",
						    G_CALLBACK
						    (ogsmd_call_status_handler_old),
						    frameworkdHandler->callCallStatus,
						    NULL);
			g_debug("registered to GSM.Call.CallStatus");
		}

		if (frameworkdHandler->pdpNetworkStatus != NULL) {
			dbus_connect_to_ogsmd_pdp();
			dbus_g_proxy_add_signal(pdpBus, "NetworkStatus",
				dbus_get_type_g_string_variant_hashtable(),
				G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(pdpBus, "NetworkStatus",
				G_CALLBACK(ogsmd_pdp_network_status_handler),
				frameworkdHandler->pdpNetworkStatus,
				NULL);
			g_debug("registered to GSM.PDP.NetworkStatus");
		}

		if (frameworkdHandler->pdpContextStatus != NULL) {
			dbus_connect_to_ogsmd_pdp();
			dbus_g_proxy_add_signal(pdpBus, "ContextStatus",
				G_TYPE_STRING,
				dbus_get_type_g_string_variant_hashtable(),
				G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(pdpBus, "ContextStatus",
				G_CALLBACK(ogsmd_pdp_context_status_handler),
				frameworkdHandler->pdpContextStatus,
				NULL);
			g_debug("registered to GSM.PDP.ContextStatus");
		}

        // NOTE ported to new signal handling method
		if (frameworkdHandler->deviceIdleNotifierState != NULL) {
			dbus_connect_to_odeviced_idle_notifier();
			dbus_g_proxy_add_signal(odevicedIdleNotifierBus,
						"State", G_TYPE_STRING,
						G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(odevicedIdleNotifierBus,
						    "State",
						    G_CALLBACK
						    (odeviced_idle_notifier_state_handler_old),
						    frameworkdHandler->deviceIdleNotifierState,
						    NULL);
			g_debug("registered to Device.IdleNotifier.State");
		}

		if (frameworkdHandler->devicePowerSupplyStatus != NULL) {
			dbus_connect_to_odeviced_power_supply();
			dbus_g_proxy_add_signal(odevicedPowerSupplyBus,
					"Status", G_TYPE_STRING,
					G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(odevicedPowerSupplyBus,
					"Status", G_CALLBACK
					(odeviced_power_supply_power_status_handler),
					frameworkdHandler->devicePowerSupplyStatus,
					NULL);
			g_debug("registered to Device.PowerSupply.Status");
		}

		if (frameworkdHandler->devicePowerSupplyCapacity != NULL) {
			dbus_connect_to_odeviced_power_supply();
			dbus_g_proxy_add_signal(odevicedPowerSupplyBus,
					"Capacity", G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(odevicedPowerSupplyBus,
					"Capacity", G_CALLBACK
					(odeviced_power_supply_capacity_handler),
					frameworkdHandler->devicePowerSupplyCapacity, NULL);
			g_debug("registered to Device.PowerSupply.Capacity.");
		}

		if (frameworkdHandler->deviceWakeupTimeChanged != NULL) {
			dbus_connect_to_odeviced_realtime_clock();
			dbus_g_proxy_add_signal(odevicedRealtimeClockBus,
					"WakeupTimeChanged", G_TYPE_INT,
					G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(odevicedRealtimeClockBus,
					"WakeupTimeChanged", G_CALLBACK
					(odeviced_realtime_clock_wakeup_time_changed_handler),
					frameworkdHandler->devicePowerSupplyCapacity, NULL);
			g_debug("registered to Device.RealtimeClock.WakeupTimeChanged");
		}

        // NOTE ported to new signal handling method
		if (frameworkdHandler->deviceInputEvent != NULL) {
			dbus_connect_to_odeviced_input();
			dbus_g_proxy_add_signal(odevicedInputBus,
					"Event", G_TYPE_STRING, G_TYPE_STRING,
					G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(odevicedInputBus,
					"Event", G_CALLBACK
					(odeviced_input_event_handler_old),
					frameworkdHandler->deviceInputEvent, NULL);
			g_debug("registered to Device.Input.Event");
		}

		if (frameworkdHandler->incomingUssd != NULL) {
			dbus_connect_to_ogsmd_network();
			dbus_g_proxy_add_signal(networkBus, "IncomingUssd",
						G_TYPE_STRING, G_TYPE_STRING,
						G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(networkBus, "IncomingUssd",
						    G_CALLBACK
						    (ogsmd_network_incoming_ussd_handler),
						    frameworkdHandler->incomingUssd,
						    NULL);
			g_debug("registered to GSM.Network.IncomingUssd");
		}

		if (frameworkdHandler->incomingMessageReceipt != NULL) {
			dbus_connect_to_ogsmd_sms();
			dbus_g_proxy_add_signal(smsBus,
						"IncomingMessageReceipt",
						G_TYPE_STRING, G_TYPE_STRING,
						dbus_get_type_g_string_variant_hashtable
						(), G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(networkBus,
						    "IncomingMessageReceipt",
						    G_CALLBACK
						    (ogsmd_sms_incoming_message_receipt_handler),
						    frameworkdHandler->incomingMessageReceipt,
						    NULL);
			g_debug("registered to GSM.SMS.IncomingMessageReceipt");
		}

		if (frameworkdHandler->usageResourceAvailable != NULL) {
			dbus_connect_to_ousaged();
			dbus_g_proxy_add_signal(ousagedBus, "ResourceAvailable",
						G_TYPE_STRING, G_TYPE_BOOLEAN,
						G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(ousagedBus,
						    "ResourceAvailable",
						    G_CALLBACK
						    (ousaged_resource_available_handler),
						    frameworkdHandler->usageResourceAvailable,
						    NULL);
			g_debug("registered to Usage.ResourceAvailable");
		}

		if (frameworkdHandler->usageResourceChanged != NULL) {
			dbus_connect_to_ousaged();
			dbus_g_proxy_add_signal(ousagedBus, "ResourceChanged",
						G_TYPE_STRING, G_TYPE_BOOLEAN,
						dbus_get_type_g_string_variant_hashtable
						(), G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(ousagedBus,
						    "ResourceChanged",
						    G_CALLBACK
						    (ousaged_resource_changed_handler),
						    frameworkdHandler->usageResourceChanged,
						    NULL);
			g_debug("registered to Usage.ResourceChanged");
		}

		if (frameworkdHandler->pimNewMessage != NULL) {
			dbus_connect_to_opimd_messages();
			dbus_g_proxy_add_signal(opimdMessagesBus, "NewMessage",
						G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdMessagesBus,
						    "NewMessage",
						    G_CALLBACK
						    (opimd_messages_new_message_handler),
						    frameworkdHandler->pimNewMessage,
						    NULL);
			g_debug("registered to PIM.Messages.NewMessage");
		}

		if (frameworkdHandler->pimUpdatedMessage != NULL) {
			dbus_connect_to_opimd_messages();
			dbus_g_proxy_add_signal(opimdMessagesBus,
				"UpdatedMessage", G_TYPE_STRING,
				dbus_get_type_g_string_variant_hashtable(),
						G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdMessagesBus,
				"UpdatedMessage", G_CALLBACK(
					opimd_messages_updated_message_handler),
				frameworkdHandler->pimUpdatedMessage, NULL);
			g_debug("registered to PIM.Messages.UpdatedMessage");
		}

		if (frameworkdHandler->pimDeletedMessage != NULL) {
			dbus_connect_to_opimd_messages();
			dbus_g_proxy_add_signal(opimdMessagesBus,
				"DeletedMessage",
				G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdMessagesBus,
				"DeletedMessage", G_CALLBACK(
					opimd_messages_deleted_message_handler),
				frameworkdHandler->pimDeletedMessage, NULL);
			g_debug("registered to PIM.Messages.DeletedMessage");
		}

		//if (frameworkdHandler->pimMovedMessage != NULL) {
		//	dbus_connect_to_opimd_messages();
		//	dbus_g_proxy_add_signal(pimdMessagesBus,
		//		"MovedMessage",
		//		G_TYPE_STRING, G_TYPE_STRING,
		//		G_TYPE_STRING, G_TYPE_INVALID);
		//	dbus_g_proxy_connect_signal(opimdMessagesBus,
		//		"MovedMessage", G_CALLBACK(
		//			opimd_messages_moved_message_handler),
		//		frameworkdHandler->pimMovedMessage, NULL);
		//	g_debug("registered to PIM.Messages.MovedMessage");
		//}

		if (frameworkdHandler->pimIncomingMessage != NULL) {
			dbus_connect_to_opimd_messages();
			dbus_g_proxy_add_signal(opimdMessagesBus,
						"IncomingMessage",
						G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdMessagesBus,
						    "IncomingMessage",
						    G_CALLBACK
						    (opimd_messages_incoming_message_handler),
						    frameworkdHandler->pimIncomingMessage,
						    NULL);
			g_debug("registered to PIM.Messages.IncomingMessage");
		}

		if (frameworkdHandler->pimUnreadMessages != NULL) {
			dbus_connect_to_opimd_messages();
			dbus_g_proxy_add_signal(opimdMessagesBus,
				"UnreadMessages",
				G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdMessagesBus,
				"UnreadMessages", G_CALLBACK(
				opimd_messages_unread_messages_handler),
				frameworkdHandler->pimUnreadMessages, NULL);
			g_debug("registered to PIM.Messages.UnreadMessages");
		}

		if (frameworkdHandler->pimNewContact != NULL) {
			dbus_connect_to_opimd_contacts();
			dbus_g_proxy_add_signal(opimdContactsBus,
				"NewContact",
				G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdContactsBus,
				"NewContact", G_CALLBACK(
				opimd_contacts_new_contact_handler),
				frameworkdHandler->pimNewContact, NULL);
			g_debug("registered to PIM.Contacts.NewContact");
		}

		if (frameworkdHandler->pimUpdatedContact != NULL) {
			dbus_connect_to_opimd_contacts();
			dbus_g_proxy_add_signal(opimdContactsBus,
				"UpdatedContact", G_TYPE_STRING,
				dbus_get_type_g_string_variant_hashtable(),
				G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdContactsBus,
				"UpdatedContact", G_CALLBACK(
					opimd_contacts_updated_contact_handler),
				frameworkdHandler->pimUpdatedContact, NULL);
			g_debug("registered to PIM.Contacts.UpdatedContact");
		}

		if (frameworkdHandler->pimDeletedContact != NULL) {
			dbus_connect_to_opimd_contacts();
			dbus_g_proxy_add_signal(opimdContactsBus,
				"DeletedContact",
				G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdContactsBus,
				"DeletedContact", G_CALLBACK(
					opimd_contacts_deleted_contact_handler),
				frameworkdHandler->pimDeletedContact, NULL);
			g_debug("registered to PIM.Contacts.DeletedContact");
		}

		if (frameworkdHandler->pimNewCall != NULL) {
			dbus_connect_to_opimd_calls();
			dbus_g_proxy_add_signal(opimdCallsBus,
					"NewCall",
					G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdCallsBus,
					"NewCall", G_CALLBACK(
						opimd_calls_new_call_handler),
					frameworkdHandler->pimNewCall, NULL);
			g_debug("registered to PIM.Calls.NewCall");
		}

		if (frameworkdHandler->pimIncomingCall != NULL) {
			dbus_connect_to_opimd_calls();
			dbus_g_proxy_add_signal(opimdCallsBus,
					"IncomingCall",
					G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdCallsBus,
					"IncomingCall", G_CALLBACK(
						opimd_calls_new_call_handler),
					frameworkdHandler->pimIncomingCall, NULL);
			g_debug("registered to PIM.Calls.IncomingCall");
		}

		if (frameworkdHandler->pimNewMissedCalls != NULL) {
			dbus_connect_to_opimd_calls();
			dbus_g_proxy_add_signal(opimdCallsBus,
				"NewMissedCalls",
				G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdCallsBus,
				"NewMissedCalls", G_CALLBACK(
					opimd_calls_new_missed_calls_handler),
				frameworkdHandler->pimNewMissedCalls, NULL);
			g_debug("registered to PIM.Calls.NewMissedCalls");
		}

		if (frameworkdHandler->pimUnfinishedTasks != NULL) {
			dbus_connect_to_opimd_tasks();
			dbus_g_proxy_add_signal(opimdTasksBus,
				"UnfinishedTasks",
				G_TYPE_INT, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opimdTasksBus,
				"UnfinishedTasks", G_CALLBACK(
					opimd_tasks_unfinished_tasks_handler),
				frameworkdHandler->pimUnfinishedTasks, NULL);
			g_debug("registered to PIM.Tasks.UnfinishedTasks");
		}

		if (frameworkdHandler->preferencesNotify != NULL) {
			dbus_connect_to_opreferencesd();
			dbus_g_proxy_add_signal(opreferencesdPreferencesBus,
					"Notify",
					G_TYPE_STRING, G_TYPE_INVALID);
			dbus_g_proxy_connect_signal(opreferencesdPreferencesBus,
					"Notify", G_CALLBACK
					(opreferencesd_notify_handler),
					frameworkdHandler->preferencesNotify,
					NULL);
			g_debug("registered to Preferences.Notify");
		}

	}
}
