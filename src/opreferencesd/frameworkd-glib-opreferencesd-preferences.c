/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opreferencesd-preferences.h"
#include "frameworkd-glib-opreferencesd-dbus.h"
#include "dbus/preferences.h"

DBusGProxy *opreferencesdPreferencesBus = NULL;


typedef struct {
	void (*callback)(GError *, char **, gpointer);
	gpointer userdata;
} opreferencesd_get_profiles_data_t;

static void
opreferencesd_get_profiles_callback(DBusGProxy *proxy, char **profiles,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opreferencesd_get_profiles_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback) {
		if (dbus_error)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, profiles, data->userdata);
		if (error) {
			g_error_free(error);
		}
	}

	if (dbus_error)
		g_error_free(dbus_error);
	g_free(data);

}

void
opreferencesd_get_profiles(void (*callback)(GError *, char **, gpointer),
		gpointer user_data)
{
	dbus_connect_to_opreferencesd();

	opreferencesd_get_profiles_data_t *data =
		g_malloc(sizeof(opreferencesd_get_profiles_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_Preferences_get_profiles_async(
			opreferencesdPreferencesBus,
			opreferencesd_get_profiles_callback, data);
}



typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opreferencesd_get_profile_data_t;

static void opreferencesd_get_profile_callback(DBusGProxy *proxy, char *profile,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opreferencesd_get_profile_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback) {
		if (dbus_error)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, profile, data->userdata);
		if (error) {
			g_error_free(error);
		}
	}

	if (dbus_error)
		g_error_free(dbus_error);
	g_free(data);
}

void opreferencesd_get_profile(
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data)
{
	dbus_connect_to_opreferencesd();

	opreferencesd_get_profile_data_t *data =
		g_malloc(sizeof(opreferencesd_get_profile_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_Preferences_get_profile_async(
			opreferencesdPreferencesBus,
			opreferencesd_get_profile_callback, data);
}

typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opreferencesd_set_profile_data_t;

void opreferencesd_set_profile_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer user_data)
{
	(void)proxy;
	opreferencesd_set_profile_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback) {
		if (dbus_error)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error) {
			g_error_free(error);
		}
	}

	if (dbus_error)
		g_error_free(dbus_error);
	g_free(data);
}


void opreferencesd_set_profile(const char *profile,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	dbus_connect_to_opreferencesd();

	opreferencesd_set_profile_data_t *data =
		g_malloc(sizeof(opreferencesd_set_profile_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_Preferences_set_profile_async(
			opreferencesdPreferencesBus, profile,
			opreferencesd_set_profile_callback, data);
}

void opreferencesd_notify_handler(DBusGProxy *proxy, const char *profile,
		gpointer user_data)
{
	(void)proxy;
	void (*callback)(const char *) = NULL;

	callback = user_data;
	if (callback)
		(*callback)(profile);
}



