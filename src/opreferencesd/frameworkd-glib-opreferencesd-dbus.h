/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */


#ifndef FRAMEWWORKD_GLIB_OPREFERENCES_DBUS_H
#define FRAMEWWORKD_GLIB_OPREFERENCES_DBUS_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "../frameworkd-glib-dbus.h"

G_BEGIN_DECLS
#define OPREFERENCESD_BUS "org.freesmartphone.opreferencesd"
#define PREFERENCES_BUS_PATH "/org/freesmartphone/Preferences"
#define PREFERENCES_INTERFACE "org.freesmartphone.Preferences"


void dbus_connect_to_opreferencesd();


G_END_DECLS
#endif

