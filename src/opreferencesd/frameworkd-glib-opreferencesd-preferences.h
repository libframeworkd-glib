/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_OPREFEENCES_PREFERENCES_H
#define FRAMEWORKD_GLIB_OPREFEENCES_PREFERENCES_H

void opreferencesd_get_profiles(
		void (*callback)(GError *, char **, gpointer),
		gpointer user_data);
void opreferencesd_set_profile(const char *profile,
		void (*callback)(GError *, gpointer),
		gpointer user_data);
void opreferencesd_get_profile(
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data);

void opreferencesd_notify_handler(DBusGProxy *proxy, const char *profile,
		gpointer user_data);


extern DBusGProxy *opreferencesdPreferencesBus;

#endif

