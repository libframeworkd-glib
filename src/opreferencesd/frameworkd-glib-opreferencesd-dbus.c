/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opreferencesd-dbus.h"
#include "frameworkd-glib-opreferencesd-preferences.h"

void
dbus_connect_to_opreferencesd()
{
	if (opreferencesdPreferencesBus == NULL)
		opreferencesdPreferencesBus =
			dbus_connect_to_interface(OPREFERENCESD_BUS,
						  PREFERENCES_BUS_PATH,
						  PREFERENCES_INTERFACE,
						  "Preferences");
}


