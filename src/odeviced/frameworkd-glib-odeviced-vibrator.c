/*
 *  Copyright (C) 2008
 *      Authors (alphabetical) :
 *              Marc-Olivier Barre <marco@marcochapeau.org>
 *              Julien Cassignol <ainulindale@gmail.com>
 *              quickdev
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-odeviced-dbus.h"
#include "dbus/vibrator.h"

DBusGProxy *odevicedVibratorBus = NULL;

typedef struct {
    void (*callback) (GError *, gpointer);
    gpointer userdata;
} odeviced_vibrator_vibrate_pattern_data_t;

static void
odeviced_vibrator_vibrate_pattern_callback(DBusGProxy * bus, GError * dbus_error,
                      gpointer userdata)
{
    (void)bus;
    odeviced_vibrator_vibrate_pattern_data_t *data = userdata;
    GError *error = NULL;

    if (data->callback != NULL) {
        if (dbus_error != NULL)
            error = dbus_handle_errors(dbus_error);

        data->callback(error, data->userdata);
        if (error != NULL)
            g_error_free(error);
    }

    if (dbus_error != NULL)
        g_error_free(dbus_error);
    g_free(data);
}

void odeviced_vibrator_vibrate_pattern(int pulses, int on_duration, int off_duration, int strength,
    void (*callback) (GError *, gpointer), gpointer userdata)
{
    dbus_connect_to_odeviced_vibrator();

    odeviced_vibrator_vibrate_pattern_data_t *data =
        g_malloc(sizeof(odeviced_vibrator_vibrate_pattern_data_t));
    data->callback = callback;
    data->userdata = userdata;

    org_freesmartphone_Device_Vibrator_vibrate_pattern_async(odevicedVibratorBus,
        pulses, on_duration, off_duration, strength,
        odeviced_vibrator_vibrate_pattern_callback,
        data);
}

typedef struct {
    void (*callback) (GError *, gpointer);
    gpointer userdata;
} odeviced_vibrator_vibrate_data_t;

static void
odeviced_vibrator_vibrate_callback(DBusGProxy * bus, GError * dbus_error,
                      gpointer userdata)
{
    (void)bus;
    odeviced_vibrator_vibrate_data_t *data = userdata;
    GError *error = NULL;

    if (data->callback != NULL) {
        if (dbus_error != NULL)
            error = dbus_handle_errors(dbus_error);

        data->callback(error, data->userdata);
        if (error != NULL)
            g_error_free(error);
    }

    if (dbus_error != NULL)
        g_error_free(dbus_error);
    g_free(data);
}

void odeviced_vibrator_vibrate(int duration, int strength,
    void (*callback) (GError *, gpointer), gpointer userdata)
{
    dbus_connect_to_odeviced_vibrator();

    odeviced_vibrator_vibrate_data_t *data =
        g_malloc(sizeof(odeviced_vibrator_vibrate_data_t));
    data->callback = callback;
    data->userdata = userdata;

    org_freesmartphone_Device_Vibrator_vibrate_async(odevicedVibratorBus,
        duration, strength,
        odeviced_vibrator_vibrate_callback,
        data);
}

typedef struct {
    void (*callback) (GError *, gpointer);
    gpointer userdata;
} odeviced_vibrator_stop_data_t;

static void
odeviced_vibrator_stop_callback(DBusGProxy * bus, GError * dbus_error,
                      gpointer userdata)
{
    (void)bus;
    odeviced_vibrator_stop_data_t *data = userdata;
    GError *error = NULL;

    if (data->callback != NULL) {
        if (dbus_error != NULL)
            error = dbus_handle_errors(dbus_error);

        data->callback(error, data->userdata);
        if (error != NULL)
            g_error_free(error);
    }

    if (dbus_error != NULL)
        g_error_free(dbus_error);
    g_free(data);
}

void odeviced_vibrator_stop(void (*callback) (GError *, gpointer), gpointer userdata)
{
    dbus_connect_to_odeviced_vibrator();

    odeviced_vibrator_stop_data_t *data =
        g_malloc(sizeof(odeviced_vibrator_stop_data_t));
    data->callback = callback;
    data->userdata = userdata;

    org_freesmartphone_Device_Vibrator_stop_async(odevicedVibratorBus,
        odeviced_vibrator_stop_callback,
        data);
}

