/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_ODEVICED_INPUT_H
#define FRAMEWORKD_GLIB_ODEVICED_INPUT_H

#include <glib.h>
#include <dbus/dbus-glib.h>


void odeviced_input_event_handler_old(DBusGProxy *proxy,
		char *name, char *action, int duration, gpointer userdata);

typedef void (*odeviced_input_event_callback)(gpointer userdata, char *source, char *action, int duration);

gpointer odeviced_input_event_connect(odeviced_input_event_callback callback, gpointer userdata);
void odeviced_input_event_disconnect(gpointer callback_data);

extern DBusGProxy *odevicedInputBus;

#endif

