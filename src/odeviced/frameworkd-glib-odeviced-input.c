/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-odeviced-input.h"
#include "frameworkd-glib-odeviced-dbus.h"
#include "dbus/input.h"

DBusGProxy *odevicedInputBus = NULL;

void odeviced_input_event_handler_old(DBusGProxy *proxy,
		char *name, char *action, int duration, gpointer userdata)
{
	(void)proxy;
	void (*callback) (char *source, char *action, int duration) = NULL;

	callback = userdata;

	if (callback != NULL) {
		(*callback)(name, action, duration);
	}
}


static void odeviced_input_event_handler(DBusGProxy *proxy,
        char *name, char *action, int duration, gpointer userdata)
{
    (void)proxy;
    gpointer* data = userdata;
    odeviced_input_event_callback callback = data[0];

    if (callback != NULL)
        (callback)(data[1], name, action, duration);
}

gpointer odeviced_input_event_connect(odeviced_input_event_callback callback, gpointer userdata)
{
    static gboolean signal_added = FALSE;

    dbus_connect_to_odeviced_input();

    if (!signal_added) {
        dbus_g_proxy_add_signal(odevicedInputBus,
            "Event", G_TYPE_STRING, G_TYPE_STRING,
            G_TYPE_INT, G_TYPE_INVALID);
        signal_added = TRUE;
    }

    gpointer* data = g_new0(gpointer, 2);
    data[0] = callback;
    data[1] = userdata;

    dbus_g_proxy_connect_signal(odevicedInputBus,
            "Event", G_CALLBACK (odeviced_input_event_handler),
            data, NULL);

    g_debug("registered callback %p to Device.Input.Event", data);
    return data;
}

void odeviced_input_event_disconnect(gpointer callback_data)
{
    dbus_g_proxy_disconnect_signal(odevicedInputBus,
            "Event", G_CALLBACK(odeviced_input_event_handler),
            callback_data);

    g_free(callback_data);
    g_debug("unregistered callback %p from Device.Input.Event", callback_data);
}
