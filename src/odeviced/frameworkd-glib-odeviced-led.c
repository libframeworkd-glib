/*
 *  Copyright (C) 2010
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-odeviced-led.h"
#include "frameworkd-glib-odeviced-dbus.h"
#include "dbus/led.h"


typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} odeviced_led_data_t;

static void
odeviced_led_callback(DBusGProxy *proxy, GError *dbus_error, gpointer userdata)
{
	(void) proxy;
	GError *error = NULL;

	odeviced_led_data_t *data = userdata;
	if (data->callback) {
		if (dbus_error) {
			error = dbus_handle_errors(dbus_error);
		}
		data->callback(error, data->userdata);
		if (error) {
			g_error_free(error);
		}
	}
	if (dbus_error)
		g_error_free(dbus_error);
	free(data);
}

void
odeviced_led_set_brightness(const char* led, int brightness,
			    void (*callback)(GError *, gpointer), gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_odeviced_led(led);
	if (!proxy)
		return;
	odeviced_led_data_t *data = g_malloc(sizeof(odeviced_led_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_Device_LED_set_brightness_async(proxy, brightness,
						odeviced_led_callback, data);
}

void
odeviced_led_set_blinking(const char* led, int on_duration, int off_duration,
			  void (*callback)(GError *, gpointer), gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_odeviced_led(led);
	if (!proxy)
		return;
	odeviced_led_data_t *data = g_malloc(sizeof(odeviced_led_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_Device_LED_set_blinking_async(proxy, on_duration,
				off_duration, odeviced_led_callback, data);
}

void
odeviced_led_blink_seconds(const char* led, int seconds, int on_duration, int off_duration,
			   void (*callback)(GError *, gpointer), gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_odeviced_led(led);
	if (!proxy)
		return;
	odeviced_led_data_t *data = g_malloc(sizeof(odeviced_led_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_Device_LED_blink_seconds_async(proxy, seconds,
			on_duration, off_duration, odeviced_led_callback, data);
}
