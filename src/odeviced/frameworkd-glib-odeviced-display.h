/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */


#ifndef FRAMEWORKD_GLIB_ODEVICED_DISPLAY_H
#define FRAMEWORKD_GLIB_ODEVICED_DISPLAY_H

#include <glib.h>
#include <dbus/dbus-glib.h>

void odeviced_display_get_info(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata);
void odeviced_display_get_brightness(void (*callback)(GError *, int, gpointer),
		gpointer userdata);
void odeviced_display_set_brightness(const int brightness,
		void (*callback)(GError *, gpointer),
		gpointer userdata);
void odeviced_display_get_backlight(void (*callback)(GError *, gboolean, gpointer),
			gpointer userdata);
void odeviced_display_set_backlight(gboolean power,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

extern DBusGProxy *odevicedDisplayBus;

#endif

