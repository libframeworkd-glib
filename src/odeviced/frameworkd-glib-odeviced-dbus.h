/*
 *  Copyright (C) 2008, 2009
 *      Authors (alphabetical) :
 *              Marc-Olivier Barre <marco@marcochapeau.org>
 *              Julien Cassignol <ainulindale@gmail.com>
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *              quickdev
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_ODEVICED_DBUS_H
#define FRAMEWORKD_GLIB_ODEVICED_DBUS_H

#include <glib.h>
#include <dbus/dbus-glib.h>

#include "../frameworkd-glib-dbus.h"

G_BEGIN_DECLS
#define DEBUG
#define ODEVICED_BUS         "org.freesmartphone.odeviced"
#define DEVICE_IDLE_NOTIFIER_BUS_PATH           "/org/freesmartphone/Device/IdleNotifier/0"
#define DEVICE_IDLE_NOTIFIER_INTERFACE		"org.freesmartphone.Device.IdleNotifier"
#define DEVICE_POWER_SUPPLY_BUS_PATH            "/org/freesmartphone/Device/PowerSupply"
#define DEVICE_POWER_SUPPLY_INTERFACE		"org.freesmartphone.Device.PowerSupply"
#define DEVICE_AUDIO_BUS_PATH                   "/org/freesmartphone/Device/Audio"
#define DEVICE_AUDIO_INTERFACE                  "org.freesmartphone.Device.Audio"
#define DEVICE_REALTIME_CLOCK_BUS_PATH          "/org/freesmartphone/Device/RealtimeClock"
#define DEVICE_REALTIME_CLOCK_INTERFACE         "org.freesmartphone.Device.RealtimeClock"
#define DEVICE_DISPLAY_BUS_PATH                 "/org/freesmartphone/Device/Display/0"
#define DEVICE_DISPLAY_INTERFACE                "org.freesmartphone.Device.Display"
#define DEVICE_INPUT_BUS_PATH                   "/org/freesmartphone/Device/Input"
#define DEVICE_INPUT_INTERFACE                  "org.freesmartphone.Device.Input"
#define DEVICE_VIBRATOR_BUS_PATH                "/org/freesmartphone/Device/Vibrator/0"
#define DEVICE_VIBRATOR_INTERFACE               "org.freesmartphone.Device.Vibrator"
#define DEVICE_LED_BUS_PATH                     "/org/freesmartphone/Device/LED/"
#define DEVICE_LED_INTERFACE                    "org.freesmartphone.Device.LED"


void dbus_connect_to_odeviced_idle_notifier();

void dbus_connect_to_odeviced_power_supply();

void dbus_connect_to_odeviced_audio();

void dbus_connect_to_odeviced_realtime_clock();

void dbus_connect_to_odeviced_display();

void dbus_connect_to_odeviced_input();

void dbus_connect_to_odeviced_vibrator();

DBusGProxy *dbus_connect_to_odeviced_led(const char *led);

G_END_DECLS
#endif

