/*
 *  Copyright (C) 2008
 *      Authors (alphabetical) :
 *              Marc-Olivier Barre <marco@marcochapeau.org>
 *              Julien Cassignol <ainulindale@gmail.com>
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *              quickdev
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-odeviced-dbus.h"
#include "frameworkd-glib-odeviced-idlenotifier.h"
#include "frameworkd-glib-odeviced-powersupply.h"
#include "frameworkd-glib-odeviced-audio.h"
#include "frameworkd-glib-odeviced-realtimeclock.h"
#include "frameworkd-glib-odeviced-display.h"
#include "frameworkd-glib-odeviced-input.h"
#include "frameworkd-glib-odeviced-vibrator.h"

void
dbus_connect_to_odeviced_idle_notifier()
{
	if (odevicedIdleNotifierBus == NULL)
		odevicedIdleNotifierBus =
			dbus_connect_to_interface(ODEVICED_BUS,
						  DEVICE_IDLE_NOTIFIER_BUS_PATH,
						  DEVICE_IDLE_NOTIFIER_INTERFACE,
						  "Device Idle Notifier");
}

void
dbus_connect_to_odeviced_power_supply()
{
	if (odevicedPowerSupplyBus == NULL)
		odevicedPowerSupplyBus =
			dbus_connect_to_interface(ODEVICED_BUS,
						  DEVICE_POWER_SUPPLY_BUS_PATH,
						  DEVICE_POWER_SUPPLY_INTERFACE,
						  "Device Power Supply");
}

void
dbus_connect_to_odeviced_audio()
{
	if (odevicedAudioBus == NULL)
		odevicedAudioBus =
			dbus_connect_to_interface(ODEVICED_BUS,
						  DEVICE_AUDIO_BUS_PATH,
						  DEVICE_AUDIO_INTERFACE,
						  "Device Audio");
}

void
dbus_connect_to_odeviced_realtime_clock()
{
	if (odevicedRealtimeClockBus == NULL)
		odevicedRealtimeClockBus =
			dbus_connect_to_interface(ODEVICED_BUS,
						  DEVICE_REALTIME_CLOCK_BUS_PATH,
						  DEVICE_REALTIME_CLOCK_INTERFACE,
						  "Device RealtimeClock");
}

void
dbus_connect_to_odeviced_display()
{
	if (odevicedDisplayBus == NULL)
		odevicedDisplayBus =
			dbus_connect_to_interface(ODEVICED_BUS,
					DEVICE_DISPLAY_BUS_PATH,
					DEVICE_DISPLAY_INTERFACE,
					"Device Display");
}

void dbus_connect_to_odeviced_input()
{
	if (odevicedInputBus == NULL)
		odevicedInputBus =
			dbus_connect_to_interface(ODEVICED_BUS,
						  DEVICE_INPUT_BUS_PATH,
						  DEVICE_INPUT_INTERFACE,
						  "Device Input");
}

void dbus_connect_to_odeviced_vibrator()
{
    if (odevicedVibratorBus == NULL)
        odevicedVibratorBus =
            dbus_connect_to_interface(ODEVICED_BUS,
                          DEVICE_VIBRATOR_BUS_PATH,
                          DEVICE_VIBRATOR_INTERFACE,
                          "Device Vibrator");
}

DBusGProxy *
dbus_connect_to_odeviced_led(const char *led)
{
	DBusGProxy *ret;
	int len = sizeof(char) * (strlen(DEVICE_LED_BUS_PATH) + strlen(led));
	char *path = malloc(len+1);
	if (!path) {
		return NULL;
	}
	snprintf(path, len, "%s%s", DEVICE_LED_BUS_PATH, led);
	ret = dbus_connect_to_interface(ODEVICED_BUS, path, DEVICE_LED_INTERFACE, led);
	free(path);
	return ret;
}
