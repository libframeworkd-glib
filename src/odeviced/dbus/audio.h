/* Generated by dbus-binding-tool; do not edit! */

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#ifndef _DBUS_GLIB_ASYNC_DATA_FREE
#define _DBUS_GLIB_ASYNC_DATA_FREE
static
#ifdef G_HAVE_INLINE
inline
#endif
void
_dbus_glib_async_data_free (gpointer stuff)
{
	g_slice_free (DBusGAsyncData, stuff);
}
#endif

#ifndef DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_Audio
#define DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_Audio

static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_get_info (DBusGProxy *proxy, GHashTable** OUT_info, GError **error)

{
  return dbus_g_proxy_call (proxy, "GetInfo", error, G_TYPE_INVALID, dbus_g_type_get_map ("GHashTable", G_TYPE_STRING, G_TYPE_VALUE), OUT_info, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_get_info_reply) (DBusGProxy *proxy, GHashTable *OUT_info, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_get_info_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  GHashTable* OUT_info;
  dbus_g_proxy_end_call (proxy, call, &error, dbus_g_type_get_map ("GHashTable", G_TYPE_STRING, G_TYPE_VALUE), &OUT_info, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_get_info_reply)data->cb) (proxy, OUT_info, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_get_info_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_get_info_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "GetInfo", org_freesmartphone_Device_Audio_get_info_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_get_available_scenarios (DBusGProxy *proxy, char *** OUT_scenarios, GError **error)

{
  return dbus_g_proxy_call (proxy, "GetAvailableScenarios", error, G_TYPE_INVALID, G_TYPE_STRV, OUT_scenarios, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_get_available_scenarios_reply) (DBusGProxy *proxy, char * *OUT_scenarios, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_get_available_scenarios_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  char ** OUT_scenarios;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_STRV, &OUT_scenarios, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_get_available_scenarios_reply)data->cb) (proxy, OUT_scenarios, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_get_available_scenarios_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_get_available_scenarios_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "GetAvailableScenarios", org_freesmartphone_Device_Audio_get_available_scenarios_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_play_sound (DBusGProxy *proxy, const char * IN_id, const gint IN_loop, const gint IN_length, GError **error)

{
  return dbus_g_proxy_call (proxy, "PlaySound", error, G_TYPE_STRING, IN_id, G_TYPE_INT, IN_loop, G_TYPE_INT, IN_length, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_play_sound_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_play_sound_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_play_sound_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_play_sound_async (DBusGProxy *proxy, const char * IN_id, const gint IN_loop, const gint IN_length, org_freesmartphone_Device_Audio_play_sound_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "PlaySound", org_freesmartphone_Device_Audio_play_sound_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_id, G_TYPE_INT, IN_loop, G_TYPE_INT, IN_length, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_stop_sound (DBusGProxy *proxy, const char * IN_id, GError **error)

{
  return dbus_g_proxy_call (proxy, "StopSound", error, G_TYPE_STRING, IN_id, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_stop_sound_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_stop_sound_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_stop_sound_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_stop_sound_async (DBusGProxy *proxy, const char * IN_id, org_freesmartphone_Device_Audio_stop_sound_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "StopSound", org_freesmartphone_Device_Audio_stop_sound_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_id, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_stop_all_sounds (DBusGProxy *proxy, GError **error)

{
  return dbus_g_proxy_call (proxy, "StopAllSounds", error, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_stop_all_sounds_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_stop_all_sounds_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_stop_all_sounds_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_stop_all_sounds_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_stop_all_sounds_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "StopAllSounds", org_freesmartphone_Device_Audio_stop_all_sounds_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_get_scenario (DBusGProxy *proxy, char ** OUT_scenario, GError **error)

{
  return dbus_g_proxy_call (proxy, "GetScenario", error, G_TYPE_INVALID, G_TYPE_STRING, OUT_scenario, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_get_scenario_reply) (DBusGProxy *proxy, char * OUT_scenario, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_get_scenario_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  char * OUT_scenario;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_STRING, &OUT_scenario, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_get_scenario_reply)data->cb) (proxy, OUT_scenario, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_get_scenario_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_get_scenario_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "GetScenario", org_freesmartphone_Device_Audio_get_scenario_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_set_scenario (DBusGProxy *proxy, const char * IN_scenario, GError **error)

{
  return dbus_g_proxy_call (proxy, "SetScenario", error, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_set_scenario_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_set_scenario_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_set_scenario_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_set_scenario_async (DBusGProxy *proxy, const char * IN_scenario, org_freesmartphone_Device_Audio_set_scenario_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "SetScenario", org_freesmartphone_Device_Audio_set_scenario_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_push_scenario (DBusGProxy *proxy, const char * IN_scenario, GError **error)

{
  return dbus_g_proxy_call (proxy, "PushScenario", error, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_push_scenario_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_push_scenario_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_push_scenario_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_push_scenario_async (DBusGProxy *proxy, const char * IN_scenario, org_freesmartphone_Device_Audio_push_scenario_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "PushScenario", org_freesmartphone_Device_Audio_push_scenario_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_pull_scenario (DBusGProxy *proxy, char ** OUT_scenario, GError **error)

{
  return dbus_g_proxy_call (proxy, "PullScenario", error, G_TYPE_INVALID, G_TYPE_STRING, OUT_scenario, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_pull_scenario_reply) (DBusGProxy *proxy, char * OUT_scenario, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_pull_scenario_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  char * OUT_scenario;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_STRING, &OUT_scenario, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_pull_scenario_reply)data->cb) (proxy, OUT_scenario, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_pull_scenario_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_pull_scenario_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "PullScenario", org_freesmartphone_Device_Audio_pull_scenario_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_save_scenario (DBusGProxy *proxy, const char * IN_scenario, GError **error)

{
  return dbus_g_proxy_call (proxy, "SaveScenario", error, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_save_scenario_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_save_scenario_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_save_scenario_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_save_scenario_async (DBusGProxy *proxy, const char * IN_scenario, org_freesmartphone_Device_Audio_save_scenario_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "SaveScenario", org_freesmartphone_Device_Audio_save_scenario_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_STRING, IN_scenario, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_get_volume (DBusGProxy *proxy, guchar* OUT_volume, GError **error)

{
  return dbus_g_proxy_call (proxy, "GetVolume", error, G_TYPE_INVALID, G_TYPE_UCHAR, OUT_volume, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_get_volume_reply) (DBusGProxy *proxy, guchar OUT_volume, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_get_volume_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  guchar OUT_volume;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_UCHAR, &OUT_volume, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_get_volume_reply)data->cb) (proxy, OUT_volume, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_get_volume_async (DBusGProxy *proxy, org_freesmartphone_Device_Audio_get_volume_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "GetVolume", org_freesmartphone_Device_Audio_get_volume_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_Audio_set_volume (DBusGProxy *proxy, const guchar IN_volume, GError **error)

{
  return dbus_g_proxy_call (proxy, "SetVolume", error, G_TYPE_UCHAR, IN_volume, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_Audio_set_volume_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_Audio_set_volume_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_Audio_set_volume_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_Audio_set_volume_async (DBusGProxy *proxy, const guchar IN_volume, org_freesmartphone_Device_Audio_set_volume_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "SetVolume", org_freesmartphone_Device_Audio_set_volume_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_UCHAR, IN_volume, G_TYPE_INVALID);
}
#endif /* defined DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_Audio */

G_END_DECLS
