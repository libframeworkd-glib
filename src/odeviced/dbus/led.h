/* Generated by dbus-binding-tool; do not edit! */

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#ifndef _DBUS_GLIB_ASYNC_DATA_FREE
#define _DBUS_GLIB_ASYNC_DATA_FREE
static
#ifdef G_HAVE_INLINE
inline
#endif
void
_dbus_glib_async_data_free (gpointer stuff)
{
	g_slice_free (DBusGAsyncData, stuff);
}
#endif

#ifndef DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_LED
#define DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_LED

static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_LED_set_brightness (DBusGProxy *proxy, const gint IN_brightness, GError **error)

{
  return dbus_g_proxy_call (proxy, "SetBrightness", error, G_TYPE_INT, IN_brightness, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_LED_set_brightness_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_LED_set_brightness_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_LED_set_brightness_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_LED_set_brightness_async (DBusGProxy *proxy, const gint IN_brightness, org_freesmartphone_Device_LED_set_brightness_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "SetBrightness", org_freesmartphone_Device_LED_set_brightness_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INT, IN_brightness, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_LED_set_blinking (DBusGProxy *proxy, const gint IN_on_duration, const gint IN_off_duration, GError **error)

{
  return dbus_g_proxy_call (proxy, "SetBlinking", error, G_TYPE_INT, IN_on_duration, G_TYPE_INT, IN_off_duration, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_LED_set_blinking_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_LED_set_blinking_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_LED_set_blinking_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_LED_set_blinking_async (DBusGProxy *proxy, const gint IN_on_duration, const gint IN_off_duration, org_freesmartphone_Device_LED_set_blinking_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "SetBlinking", org_freesmartphone_Device_LED_set_blinking_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INT, IN_on_duration, G_TYPE_INT, IN_off_duration, G_TYPE_INVALID);
}
static
#ifdef G_HAVE_INLINE
inline
#endif
gboolean
org_freesmartphone_Device_LED_blink_seconds (DBusGProxy *proxy, const gint IN_seconds, const gint IN_on_duration, const gint IN_off_duration, GError **error)

{
  return dbus_g_proxy_call (proxy, "BlinkSeconds", error, G_TYPE_INT, IN_seconds, G_TYPE_INT, IN_on_duration, G_TYPE_INT, IN_off_duration, G_TYPE_INVALID, G_TYPE_INVALID);
}

typedef void (*org_freesmartphone_Device_LED_blink_seconds_reply) (DBusGProxy *proxy, GError *error, gpointer userdata);

static void
org_freesmartphone_Device_LED_blink_seconds_async_callback (DBusGProxy *proxy, DBusGProxyCall *call, void *user_data)
{
  DBusGAsyncData *data = (DBusGAsyncData*) user_data;
  GError *error = NULL;
  dbus_g_proxy_end_call (proxy, call, &error, G_TYPE_INVALID);
  (*(org_freesmartphone_Device_LED_blink_seconds_reply)data->cb) (proxy, error, data->userdata);
  return;
}

static
#ifdef G_HAVE_INLINE
inline
#endif
DBusGProxyCall*
org_freesmartphone_Device_LED_blink_seconds_async (DBusGProxy *proxy, const gint IN_seconds, const gint IN_on_duration, const gint IN_off_duration, org_freesmartphone_Device_LED_blink_seconds_reply callback, gpointer userdata)

{
  DBusGAsyncData *stuff;
  stuff = g_slice_new (DBusGAsyncData);
  stuff->cb = G_CALLBACK (callback);
  stuff->userdata = userdata;
  return dbus_g_proxy_begin_call (proxy, "BlinkSeconds", org_freesmartphone_Device_LED_blink_seconds_async_callback, stuff, _dbus_glib_async_data_free, G_TYPE_INT, IN_seconds, G_TYPE_INT, IN_on_duration, G_TYPE_INT, IN_off_duration, G_TYPE_INVALID);
}
#endif /* defined DBUS_GLIB_CLIENT_WRAPPERS_org_freesmartphone_Device_LED */

G_END_DECLS
