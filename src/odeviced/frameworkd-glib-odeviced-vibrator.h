/*
 *  Copyright (C) 2008
 *      Authors (alphabetical) :
 *              quickdev
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_ODEVICED_VIBRATOR_H
#define FRAMEWORKD_GLIB_ODEVICED_VIBRATOR_H

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

void odeviced_vibrator_vibrate_pattern(int pulses, int on_duration, int off_duration, int strength,
    void (*callback) (GError *, gpointer), gpointer userdata);

void odeviced_vibrator_vibrate(int duration, int strength,
    void (*callback) (GError *, gpointer), gpointer userdata);

void odeviced_vibrator_stop(void (*callback) (GError *, gpointer), gpointer userdata);


extern DBusGProxy *odevicedVibratorBus;

G_END_DECLS
#endif
