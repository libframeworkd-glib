/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWWORKD_GLIB_ODEVICED_REALTIME_CLOCK_H
#define FRAMEWWORKD_GLIB_ODEVICED_REALTIME_CLOCK_H

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

void odeviced_realtime_clock_wakeup_time_changed_handler(DBusGProxy *proxy,
		const int alarm, gpointer user_data);

extern DBusGProxy *odevicedRealtimeClockBus;

G_END_DECLS
#endif


