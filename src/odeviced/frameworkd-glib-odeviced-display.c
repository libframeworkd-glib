/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *		Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-odeviced-display.h"
#include "frameworkd-glib-odeviced-dbus.h"
#include "dbus/display.h"

DBusGProxy *odevicedDisplayBus = NULL;


typedef struct {
	void (*callback)(GError *, GHashTable *, gpointer);
	gpointer userdata;
} odeviced_display_get_info_data_t;

static void
odeviced_display_get_info_callback(DBusGProxy *proxy, GHashTable *properties,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	odeviced_display_get_info_data_t *data = userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, properties, data->userdata);

		if (error != NULL) {
			g_error_free(error);
		}
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
odeviced_display_get_info(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata)
{
	odeviced_display_get_info_data_t *data =
		g_malloc(sizeof(odeviced_display_get_info_data_t));
	data->callback = callback;
	data->userdata = userdata;

	dbus_connect_to_odeviced_display();

	org_freesmartphone_Device_Display_get_info_async(odevicedDisplayBus,
			odeviced_display_get_info_callback, data);
}


typedef struct {
	void (*callback)(GError *, int, gpointer);
	gpointer userdata;
} odeviced_display_get_brightness_data_t;

static void
odeviced_display_get_brightness_callback(DBusGProxy *proxy, int brightness,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	odeviced_display_get_brightness_data_t *data = userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, brightness, data->userdata);

		if (error != NULL) {
			g_error_free(error);
		}
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
odeviced_display_get_brightness(void (*callback)(GError *, int, gpointer),
		gpointer userdata)
{
	odeviced_display_get_brightness_data_t *data =
		g_malloc(sizeof(odeviced_display_get_brightness_data_t));
	data->callback = callback;
	data->userdata = userdata;
	dbus_connect_to_odeviced_display();
	org_freesmartphone_Device_Display_get_brightness_async(odevicedDisplayBus,
			odeviced_display_get_brightness_callback, data);
}

typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} odeviced_display_set_brightness_data_t;

static void
odeviced_display_set_brightness_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer userdata)
{
	(void)proxy;
	odeviced_display_set_brightness_data_t *data = userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, data->userdata);

		if (error != NULL) {
			g_error_free(error);
		}
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
odeviced_display_set_brightness(const int brightness,
		void (*callback)(GError *, gpointer), gpointer userdata)
{
	odeviced_display_set_brightness_data_t *data =
		g_malloc(sizeof(odeviced_display_set_brightness_data_t));
	data->callback = callback;
	data->userdata = userdata;
	dbus_connect_to_odeviced_display();
	org_freesmartphone_Device_Display_set_brightness_async(odevicedDisplayBus,
			brightness, odeviced_display_set_brightness_callback, data);
}

typedef struct {
	void (*callback)(GError *, gboolean, gpointer);
	gpointer userdata;
} odeviced_display_get_backlight_data_t;

static void
odeviced_display_get_backlight_callback(DBusGProxy *proxy, gboolean power,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	odeviced_display_get_backlight_data_t *data = userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, power, data->userdata);

		if (error != NULL) {
			g_error_free(error);
		}
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
odeviced_display_get_backlight(void (*callback)(GError *, gboolean power, gpointer),
		gpointer userdata)
{
	odeviced_display_get_backlight_data_t *data =
		g_malloc(sizeof(odeviced_display_get_backlight_data_t));
	data->callback = callback;
	data->userdata = userdata;
	dbus_connect_to_odeviced_display();
	org_freesmartphone_Device_Display_get_backlight_power_async(odevicedDisplayBus,
			odeviced_display_get_backlight_callback, data);
}

typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} odeviced_display_set_backlight_data_t;

static void
odeviced_display_set_backlight_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer userdata)
{
	(void)proxy;
	odeviced_display_set_backlight_data_t *data = userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, data->userdata);

		if (error != NULL) {
			g_error_free(error);
		}
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
odeviced_display_set_backlight(gboolean power,
		void (*callback)(GError *, gpointer), gpointer userdata)
{
	odeviced_display_set_backlight_data_t *data =
		g_malloc(sizeof(odeviced_display_set_backlight_data_t));
	data->callback = callback;
	data->userdata = userdata;
	dbus_connect_to_odeviced_display();
	org_freesmartphone_Device_Display_set_backlight_power_async(odevicedDisplayBus,
			power, odeviced_display_set_backlight_callback, data);
}


