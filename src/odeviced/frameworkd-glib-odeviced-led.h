/*
 *  Copyright (C) 2010
 *      Authors (alphabetical) :
 *              Klaus 'mrmoku' Kurzmann <mok@fluxnetz.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_ODEVICED_LED_H
#define FRAMEWORKD_GLIB_ODEVICED_LED_H

#include <glib.h>
#include <dbus/dbus-glib.h>


void odeviced_led_set_brightness(const char *led, int brightness, void (*callback)(GError *, gpointer), gpointer userdata);
void odeviced_led_set_blinking(const char *led, int on_duration, int off_duration, void (*callback)(GError *, gpointer), gpointer userdata);
void odeviced_led_blink_seconds(const char *led, int seconds, int on_duration, int off_duration, void (*callback)(GError *, gpointer), gpointer userdata);

#endif