
#ifndef FRAMEWORKD_GLIB_OGSMD_PDP_H
#define FRAMEWORKD_GLIB_OGSMD_PDP_H

#include <glib.h>
#include <dbus/dbus-glib.h>

void
ogsmd_pdp_get_network_status(void (*callback)(GError *, GHashTable*, gpointer),
					      gpointer userdata);
void
ogsmd_pdp_set_credentials(const char *apn,
			  const char *username,
			  const char *password,
			  void (*callback)(GError *, gpointer),
			  gpointer userdata);
void
ogsmd_pdp_get_credentials(void (*callback)
			  (GError *, char *, char *, char *, gpointer),
			  gpointer userdata);
void
ogsmd_pdp_activate_context(void (*callback)(GError *, gpointer),
			   gpointer userdata);
void
ogsmd_pdp_deactivate_context(void (*callback)(GError *, gpointer),
			     gpointer userdata);
void
ogsmd_pdp_get_context_status(void (*callback)(GError *, char *, GHashTable *, gpointer),
			     gpointer userdata);

void
ogsmd_pdp_network_status_handler(DBusGProxy *proxy, GHashTable *status,
				 gpointer userdata);
void
ogsmd_pdp_context_status_handler(DBusGProxy *proxy, char *status,
				 GHashTable *properties, gpointer userdata);

extern DBusGProxy *pdpBus;

#endif