/*
 *  Copyright (C) 2008
 *      Authors (alphabetical) :
 *              Marc-Olivier Barre <marco@marcochapeau.org>
 *              Julien Cassignol <ainulindale@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_OGSMD_DEVICE_H
#define FRAMEWORKD_GLIB_OGSMD_DEVICE_H

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#define DBUS_DEVICE_ERROR_NOT_PRESENT "org.freesmartphone.GSM.Device.NotPresent"
#define DBUS_DEVICE_ERROR_TIMEOUT "org.freesmartphone.GSM.Device.Timeout"
#define DBUS_DEVICE_ERROR_FAILED "org.freesmartphone.GSM.Device.Failed"
#define DBUS_DEVICE_ERROR_UNSUPPORTED "org.freesmartphone.Unsupported"

#define DBUS_DEVICE_STATUS_UNKNOWN              "unknown"
#define DBUS_DEVICE_STATUS_CLOSED               "closed"
#define DBUS_DEVICE_STATUS_INITIALIZING         "initializing"
#define DBUS_DEVICE_STATUS_ALIVE_NO_SIM         "alive-no-sim"
#define DBUS_DEVICE_STATUS_ALIVE_SIM_LOCKED     "alive-sim-locked"
#define DBUS_DEVICE_STATUS_ALIVE_SIM_UNLOCKED   "alive-sim-unlocked"
#define DBUS_DEVICE_STATUS_ALIVE_SIM_READY      "alive-sim-ready"
#define DBUS_DEVICE_STATUS_ALIVE_REGISTERED     "alive-registered"
#define DBUS_DEVICE_STATUS_ALIVE_SUSPENDING     "alive-suspending"
#define DBUS_DEVICE_STATUS_ALIVE_SUSPENDED      "alive-suspended"
#define DBUS_DEVICE_STATUS_ALIVE_RESUMING       "alive-resuming"
#define DBUS_DEVICE_STATUS_ALIVE_CLOSING        "alive-closing"

#define DEVICE_ERROR g_quark_from_static_string(DEVICE_INTERFACE)
#define IS_DEVICE_ERROR(error, code) g_error_matches(error, DEVICE_ERROR, code)

typedef enum {
	DEVICE_ERROR_NOT_PRESENT = -1,
	DEVICE_ERROR_TIMEOUT = -2,
	DEVICE_ERROR_FAILED = -3,
	DEVICE_ERROR_UNSUPPORTED = -4
} DeviceErrors;

typedef enum {
    DEVICE_STATUS_UNKNOWN = 0,
    DEVICE_STATUS_CLOSED,
    DEVICE_STATUS_INITIALIZING,
    DEVICE_STATUS_ALIVE_NO_SIM,
    DEVICE_STATUS_ALIVE_SIM_LOCKED,
    DEVICE_STATUS_ALIVE_SIM_UNLOCKED,
    DEVICE_STATUS_ALIVE_SIM_READY,
    DEVICE_STATUS_ALIVE_REGISTERED,
    DEVICE_STATUS_ALIVE_SUSPENDING,
    DEVICE_STATUS_ALIVE_SUSPENDED,
    DEVICE_STATUS_ALIVE_RESUMING,
    DEVICE_STATUS_ALIVE_CLOSING
} DeviceStatus;

GError *ogsmd_device_handle_errors(GError * dbus_error);
int ogsmd_device_handle_device_status(char* status);

void
ogsmd_device_status_handler(DBusGProxy *proxy, char *status,
        gpointer userdata);


void ogsmd_device_set_antenna_power(gboolean power,
				    void (*callback) (GError *, gpointer),
				    gpointer userdata);

void
  ogsmd_device_get_antenna_power(void (*callback)

				 (GError *, gboolean power, gpointer),
				 gpointer userdata);

void
ogsmd_device_set_functionality(const char *level, gboolean autoregister,
			       const char *pin, void (*callback)(GError *, gpointer),
			       gpointer userdata);

void
ogsmd_device_get_functionality(void (*callback)(GError *, char *, gboolean,
				char *, gpointer), gpointer userdata);
void
ogsmd_device_get_info(void (*callback) (GError *, GHashTable * info, gpointer),
		      gpointer userdata);

void
  ogsmd_device_get_features(void (*callback)

			    (GError *, GHashTable * features, gpointer),
			    gpointer userdata);

void ogsmd_device_get_device_status(void (*callback) (GError *, int, gpointer),
    gpointer userdata);

void ogsmd_device_prepare_to_suspend(void (*callback) (GError *, gpointer),
				     gpointer userdata);

void ogsmd_device_recover_from_suspend(void (*callback) (GError *, gpointer),
				       gpointer userdata);

extern DBusGProxy *deviceBus;

G_END_DECLS
#endif
