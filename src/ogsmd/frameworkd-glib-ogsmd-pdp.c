
#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>

#include "frameworkd-glib-ogsmd-pdp.h"
#include "frameworkd-glib-ogsmd-dbus.h"
#include "dbus/pdp.h"

DBusGProxy *pdpBus = NULL;

/* --- org.freesmartphone.GSM.PDP.GetNetworkStatus --- */
typedef struct {
	void (*callback) (GError *, GHashTable *, gpointer);
	gpointer data;
} ogsmd_pdp_get_network_status_data_t;

static void
ogsmd_pdp_get_network_status_callback(DBusGProxy *proxy,
				      GHashTable *status,
				      GError *dbus_error,
				      gpointer userdata)
{
	(void)proxy;
	ogsmd_pdp_get_network_status_data_t *data =
		(ogsmd_pdp_get_network_status_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, status, data->data);
		if (error != NULL)
			g_error_free(error);
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
ogsmd_pdp_get_network_status(void (*callback)(GError *, GHashTable*, gpointer),
					      gpointer userdata)
{
	dbus_connect_to_ogsmd_pdp();

	ogsmd_pdp_get_network_status_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_get_network_status_data_t));
	data->callback = callback;
	data->data = userdata;

	org_freesmartphone_GSM_PDP_get_network_status_async(pdpBus,
			ogsmd_pdp_get_network_status_callback, data);
}

/* --- org.freesmartphone.GSM.PDP.SetCredentials --- */
typedef struct {
	void (*callback) (GError *, gpointer);
	gpointer data;
} ogsmd_pdp_set_credentials_data_t;

static void
ogsmd_pdp_set_credentials_callback(DBusGProxy *proxy,
				   GError *dbus_error,
				   gpointer userdata)
{
	(void)proxy;
	ogsmd_pdp_set_credentials_data_t *data =
		(ogsmd_pdp_set_credentials_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);

		data->callback(error, data->data);
		if (error != NULL)
			g_error_free(error);
	}

	if (dbus_error != NULL)
		g_error_free(dbus_error);
	g_free(data);
}

void
ogsmd_pdp_set_credentials(const char *apn,
			  const char *username,
			  const char *password,
			  void (*callback)(GError *, gpointer),
			  gpointer userdata)
{
	ogsmd_pdp_set_credentials_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_set_credentials_data_t));
	data->callback = callback;
	data->data = userdata;

	dbus_connect_to_ogsmd_pdp();

	org_freesmartphone_GSM_PDP_set_credentials_async
		(pdpBus, apn, username, password,
		 ogsmd_pdp_set_credentials_callback, data);
}

/* --- org.freesmartphone.GSM.PDP.GetCredentials --- */
typedef struct {
	void (*callback)(GError *, char *, char *, char *, gpointer);
	gpointer data;
} ogsmd_pdp_get_credentials_data_t;

static void
ogsmd_pdp_get_credentials_callback(DBusGProxy *proxy,
				   char *apn,
				   char *username,
				   char *password,
				   GError *dbus_error,
				   gpointer userdata)
{
	GError *error = NULL;
	(void)proxy;
	ogsmd_pdp_get_credentials_data_t *data =
		(ogsmd_pdp_get_credentials_data_t *)userdata;

	if (data->callback) {
		if (dbus_error) {
			error = dbus_handle_errors(dbus_error);
		}
		data->callback(error, apn, username, password, data->data);
		if (error) {
			g_error_free(error);
		}
	}
	if (dbus_error)
		g_error_free(dbus_error);
	g_free(data);
}

void
ogsmd_pdp_get_credentials(void (*callback)
			  (GError *, char *, char *, char *, gpointer),
			  gpointer userdata)
{
	ogsmd_pdp_get_credentials_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_get_credentials_data_t));
	data->callback = callback;
	data->data = userdata;

	dbus_connect_to_ogsmd_pdp();

	org_freesmartphone_GSM_PDP_get_credentials_async(pdpBus,
				ogsmd_pdp_get_credentials_callback, data);
}

/* --- org.freesmartphone.GSM.PDP.ActivateContext --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer data;
} ogsmd_pdp_activate_context_data_t;

static void
ogsmd_pdp_activate_context_callback(DBusGProxy *proxy,
				    GError *dbus_error,
				    gpointer userdata)
{
	GError *error = NULL;
	(void)proxy;
	ogsmd_pdp_activate_context_data_t *data =
		(ogsmd_pdp_activate_context_data_t *)userdata;
	if (data->callback) {
		if (dbus_error) {
			error = dbus_handle_errors(dbus_error);
		}
		data->callback(error, data->data);
		if (error) {
			g_error_free(error);
		}
	}
	if (dbus_error) {
		g_error_free(dbus_error);
	}
	g_free(data);
}

void
ogsmd_pdp_activate_context(void (*callback)(GError *, gpointer),
			   gpointer userdata)
{
	ogsmd_pdp_activate_context_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_activate_context_data_t));
	data->callback = callback;
	data->data = userdata;

	dbus_connect_to_ogsmd_pdp();

	org_freesmartphone_GSM_PDP_activate_context_async(pdpBus,
			ogsmd_pdp_activate_context_callback, data);
}

/* --- org.freesmartphone.GSM.PDP.DeactivateContext --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer data;
} ogsmd_pdp_deactivate_context_data_t;

static void
ogsmd_pdp_deactivate_context_callback(DBusGProxy *proxy,
				  GError *dbus_error,
				  gpointer userdata)
{
	GError *error = NULL;
	(void)proxy;
	ogsmd_pdp_deactivate_context_data_t *data =
		(ogsmd_pdp_deactivate_context_data_t *)userdata;
	if (data->callback) {
		if (dbus_error) {
			error = dbus_handle_errors(dbus_error);
		}
		data->callback(error, data->data);
		if (error) {
			g_error_free(error);
		}
	}
	if (dbus_error) {
		g_error_free(dbus_error);
	}
	g_free(data);
}

void
ogsmd_pdp_deactivate_context(void (*callback)(GError *, gpointer),
			     gpointer userdata)
{
	ogsmd_pdp_deactivate_context_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_deactivate_context_data_t));
	data->callback = callback;
	data->data = userdata;

	dbus_connect_to_ogsmd_pdp();

	org_freesmartphone_GSM_PDP_deactivate_context_async(pdpBus,
			ogsmd_pdp_deactivate_context_callback, data);
}

/* --- org.freesmartphone.GSM.PDP.GetContextStatus --- */
typedef struct {
	void (*callback)(GError *, char *, GHashTable *, gpointer);
	gpointer data;
} ogsmd_pdp_get_context_status_data_t;

static void
ogsmd_pdp_get_context_status_callback(DBusGProxy *proxy, char *status,
				      GHashTable *properties,
				      GError *dbus_error,
				      gpointer userdata)
{
	(void)proxy;
	GError *error = NULL;
	ogsmd_pdp_get_context_status_data_t *data =
		(ogsmd_pdp_get_context_status_data_t *)userdata;
	if (data->callback) {
		if (dbus_error) {
			dbus_handle_errors(dbus_error);
		}
		data->callback(error, status, properties, data->data);
		if (error) {
			g_error_free(error);
		}
	}
	if (dbus_error) {
		g_error_free(dbus_error);
	}
	g_free(data);
}

void
ogsmd_pdp_get_context_status(void (*callback)(GError *, char *, GHashTable *, gpointer),
			     gpointer userdata)
{
	ogsmd_pdp_get_context_status_data_t *data =
		g_malloc(sizeof(ogsmd_pdp_get_context_status_data_t));
	data->callback = callback;
	data->data = userdata;

	dbus_connect_to_ogsmd_pdp();

	org_freesmartphone_GSM_PDP_get_context_status_async(pdpBus,
			ogsmd_pdp_get_context_status_callback, data);
}

/* --- signal: org.freesmartphone.GSM.PDP.NetworkStatus --- */
void
ogsmd_pdp_network_status_handler(DBusGProxy *proxy, GHashTable *status,
				 gpointer userdata)
{
	(void)proxy;
	void (*callback)(GHashTable *) = NULL;

	callback = userdata;
	if (callback)
		(*callback)(status);
}

/* --- signal: org.freesmartphone.GSM.PDP.ContextStatus --- */
void
ogsmd_pdp_context_status_handler(DBusGProxy *proxy, char *status,
				 GHashTable *properties, gpointer userdata)
{
	(void)proxy;
	void (*callback)(char *, GHashTable *) = NULL;

	callback = userdata;
	if (callback)
		(*callback)(status, properties);
}
