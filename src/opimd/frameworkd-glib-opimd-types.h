/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_OPIMD_TYPES_H
#define FRAMEWORKD_GLIB_OPIMD_TYPES_H

#include <glib.h>
#include <dbus/dbus-glib.h>

void opimd_types_list(
		void (*callback)(GError *, char **, gpointer userdata),
		gpointer user_data);

extern DBusGProxy *opimdTypesBus;

#endif

