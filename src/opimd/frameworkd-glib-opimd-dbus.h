/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */


#ifndef FRAMEWORKD_GLIB_OPIMD_DBUS_H
#define FRAMEWORKD_GLIB_OPIMD_DBUS_H

#include <glib.h>
#include <dbus/dbus-glib.h>
#include "../frameworkd-glib-dbus.h"

G_BEGIN_DECLS
#define DEBUG
#define OPIMD_BUS                   "org.freesmartphone.opimd"
#define CONTACTS_BUS_PATH            "/org/freesmartphone/PIM/Contacts"
#define CONTACTS_INTERFACE           "org.freesmartphone.PIM.Contacts"
#define CONTACT_INTERFACE            "org.freesmartphone.PIM.Contact"
#define CONTACT_QUERY_INTERFACE      "org.freesmartphone.PIM.ContactQuery"
#define MESSAGES_BUS_PATH            "/org/freesmartphone/PIM/Messages"
#define MESSAGES_INTERFACE           "org.freesmartphone.PIM.Messages"
#define MESSAGE_INTERFACE            "org.freesmartphone.PIM.Message"
#define MESSAGE_QUERY_INTERFACE      "org.freesmartphone.PIM.MessageQuery"
#define MESSAGE_FOLDER_INTERFACE     "org.freesmartphone.PIM.MessageFolder"
#define SOURCES_BUS_PATH             "/org/freesmartphone/PIM/Sources"
#define SOURCES_INTERFACE            "org.freesmartphone.PIM.Sources"
#define SOURCE_INTERFACE             "org.freesmartphone.PIM.Source"
#define CALLS_BUS_PATH               "/org/freesmartphone/PIM/Calls"
#define CALLS_INTERFACE              "org.freesmartphone.PIM.Calls"
#define PIM_CALL_INTERFACE           "org.freesmartphone.PIM.Call"
#define CALL_QUERY_INTERFACE         "org.freesmartphone.PIM.CallQuery"
#define TYPES_BUS_PATH               "/org/freesmartphone.PIM.Types"
#define TYPES_INTERFACE              "org.freesmartphone.PIM.Types"
#define FIELDS_INTERFACE             "org.freesmartphone.PIM.Fields"
#define TASKS_BUS_PATH               "/org/freesmartphone/PIM/Tasks"
#define TASKS_INTERFACE              "org.freesmartphone.PIM.Tasks"

void dbus_connect_to_opimd_types();
DBusGProxy *dbus_connect_to_opimd_fields(const char *domain);

void dbus_connect_to_opimd_contacts();
DBusGProxy *dbus_connect_to_opimd_contact(const char *contact_path);
DBusGProxy *dbus_connect_to_opimd_contact_query(const char *query_path);

void dbus_connect_to_opimd_messages();
DBusGProxy *dbus_connect_to_opimd_message(const char *message_path);
DBusGProxy *dbus_connect_to_opimd_message_query(const char *query_path);
DBusGProxy *dbus_connect_to_opimd_message_folder(const char *folder_path);

void dbus_connect_to_opimd_sources();
DBusGProxy *dbus_connect_to_opimd_source(const char *source_path);

void dbus_connect_to_opimd_calls();
DBusGProxy *
dbus_connect_to_opimd_call(const char *call_path);
DBusGProxy *
dbus_connect_to_opimd_call_query(const char *query_path);

void dbus_connect_to_opimd_tasks();

G_END_DECLS
#endif
