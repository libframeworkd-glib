/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMWORKD_GLIB_OPIMD_CALLS_H
#define FRAMWORKD_GLIB_OPIMD_CALLS_H

#include <glib.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

void
opimd_calls_add(GHashTable *call_data,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata);

void
opimd_calls_get_single_entry_single_field(GHashTable *query, char *field,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata);

void
opimd_calls_query(GHashTable *query,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata);

void
opimd_calls_get_new_missed_calls(
		void (*callback)(GError *, const int, gpointer),
		gpointer userdata);

void
opimd_calls_new_call_handler(DBusGProxy *proxy,
		char *path,
		gpointer userdata);

void
opimd_calls_incoming_call_handler(DBusGProxy *proxy,
		char *path,
		gpointer userdata);

void
opimd_calls_new_missed_calls_handler(DBusGProxy *proxy,
		const int amount,
		gpointer userdata);


/* PIM.Call interface */

void
opimd_call_get_content(const char *call_path,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata);

void
opimd_call_get_multiple_fields(const char *call_path, const char *fieldlist,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata);

void
opimd_call_get_used_backends(const char *call_path,
		void (*callback)(GError *, char **, gpointer),
		gpointer userdata);

void
opimd_call_update(const char *call_path, GHashTable *call_data,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

void
opimd_call_delete(const char *call_path,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

/* PIM.CallQuery */

void
opimd_call_query_get_result_count(DBusGProxy *proxy,
		void (*callback)(GError *, int, gpointer),
		gpointer userdata);

void
opimd_call_query_rewind(DBusGProxy *proxy,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

void
opimd_call_query_skip(DBusGProxy *proxy, const int count,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

void
opimd_call_query_get_call_path(DBusGProxy *proxy,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata);

void
opimd_call_query_get_result(DBusGProxy *proxy,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata);

void
opimd_call_query_get_multiple_results(DBusGProxy *proxy, const int count,
		void (*callback)(GError *, GPtrArray *, gpointer),
		gpointer userdata);

void
opimd_call_query_dispose(DBusGProxy *proxy,
		void (*callback)(GError *, gpointer),
		gpointer userdata);

void
opimd_call_query_call_added_handler(DBusGProxy *proxy,
		char *call_path, gpointer userdata);


extern DBusGProxy *opimdCallsBus;

G_END_DECLS
#endif

