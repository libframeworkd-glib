/*
 *  Copyright (C) 2010
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */


#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opimd-tasks.h"
#include "frameworkd-glib-opimd-dbus.h"
#include "dbus/tasks.h"

DBusGProxy *opimdTasksBus = NULL;


void
opimd_tasks_unfinished_tasks_handler(DBusGProxy* proxy, int amount, gpointer userdata)
{
	(void)proxy;
	void (*callback) (const int) = NULL;

	callback = userdata;

	if (callback != NULL)
		(*callback)(amount);

}
