/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opimd-types.h"
#include "frameworkd-glib-opimd-dbus.h"
#include "dbus/types.h"

DBusGProxy *opimdTypesBus = NULL;


typedef struct {
	void (*callback)(GError *, char **, gpointer);
	gpointer userdata;
} opimd_types_list_data_t;

static void
opimd_types_list_callback(DBusGProxy *proxy, char **types,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opimd_types_list_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, types, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}

void
opimd_types_list(void (*callback)(GError *, char **, gpointer),
		gpointer user_data)
{
	dbus_connect_to_opimd_types();

	opimd_types_list_data_t *data =
		g_malloc(sizeof(opimd_types_list_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_PIM_Types_list_async(opimdTypesBus,
			opimd_types_list_callback, data);
}

