/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opimd-fields.h"
#include "frameworkd-glib-opimd-dbus.h"
#include "dbus/fields.h"



typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_fields_add_data_t;

static void
opimd_fields_add_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer user_data)
{
	(void)proxy;
	opimd_fields_add_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}


static void
opimd_fields_add(DBusGProxy *proxy, const char *name, const char *type,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	opimd_fields_add_data_t *data =
		g_malloc(sizeof(opimd_fields_add_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_PIM_Fields_add_field_async(proxy, name, type,
			opimd_fields_add_callback, data);
}



typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_fields_delete_data_t;

static void
opimd_fields_delete_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer user_data)
{
	(void)proxy;
	GError *error = NULL;
	opimd_fields_delete_data_t *data = user_data;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}

static void
opimd_fields_delete(DBusGProxy *proxy, const char *name,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	opimd_fields_delete_data_t *data =
		g_malloc(sizeof(opimd_fields_delete_data_t));
	data->callback = callback;
	data->userdata = user_data;
	org_freesmartphone_PIM_Fields_delete_field_async(proxy, name,
			opimd_fields_delete_callback, data);
}


typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opimd_fields_get_data_t;

static void
opimd_fields_get_callback(DBusGProxy *proxy, char *type,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opimd_fields_get_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, type, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}

static void
opimd_fields_get(DBusGProxy *proxy, const char *name,
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data)
{
	opimd_fields_get_data_t *data =
		g_malloc(sizeof(opimd_fields_get_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_PIM_Fields_get_type_async(proxy, name,
			opimd_fields_get_callback, data);
}


typedef struct {
	void (*callback)(GError *, GHashTable *, gpointer);
	gpointer userdata;
} opimd_fields_list_data_t;

static void
opimd_fields_list_callback(DBusGProxy *proxy, GHashTable *fields,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opimd_fields_list_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, fields, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}

static void
opimd_fields_list(DBusGProxy *proxy,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer user_data)
{
	opimd_fields_list_data_t *data =
		g_malloc(sizeof(opimd_fields_list_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_PIM_Fields_list_fields_async(proxy,
			opimd_fields_list_callback, data);
}

typedef struct {
	void (*callback)(GError *, char **, gpointer);
	gpointer userdata;
} opimd_fields_list_fields_with_type_data_t;

static void
opimd_fields_list_fields_with_type_callback(DBusGProxy *proxy, char **fields,
		GError *dbus_error, gpointer user_data)
{
	(void)proxy;
	opimd_fields_list_fields_with_type_data_t *data = user_data;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, fields, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}

static void
opimd_fields_list_fields_with_type(DBusGProxy *proxy, const char *type,
		void (*callback)(GError *, char **, gpointer),
		gpointer user_data)
{
	opimd_fields_list_fields_with_type_data_t *data =
		g_malloc(sizeof(opimd_fields_list_fields_with_type_data_t));
	data->callback = callback;
	data->userdata = user_data;

	org_freesmartphone_PIM_Fields_list_fields_with_type_async(proxy, type,
			opimd_fields_list_fields_with_type_callback, data);
}

void opimd_contacts_fields_add(const char *name, const char *type,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(CONTACTS_BUS_PATH);
	opimd_fields_add(proxy, name, type, callback, user_data);
}

void opimd_messages_fields_add(const char *name, const char *type,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(MESSAGES_BUS_PATH);
	opimd_fields_add(proxy, name, type, callback, user_data);
}

void opimd_contacts_fields_delete(const char *name,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(CONTACTS_BUS_PATH);
	opimd_fields_delete(proxy, name, callback, user_data);
}

void opimd_messages_fields_delete(const char *name,
		void (*callback)(GError *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(MESSAGES_BUS_PATH);
	opimd_fields_delete(proxy, name, callback, user_data);
}

void opimd_contacts_fields_get(const char *name,
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(CONTACTS_BUS_PATH);
	opimd_fields_get(proxy, name, callback, user_data);
}

void opimd_messages_fields_get(const char *name,
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(MESSAGES_BUS_PATH);
	opimd_fields_get(proxy, name, callback, user_data);
}

void opimd_contacts_fields_list(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(CONTACTS_BUS_PATH);
	opimd_fields_list(proxy, callback, user_data);
}

void opimd_messages_fields_list(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(MESSAGES_BUS_PATH);
	opimd_fields_list(proxy, callback, user_data);
}

void opimd_contacts_fields_list_fields_with_type(const char *type,
		void (*callback)(GError *, char **, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(CONTACTS_BUS_PATH);
	opimd_fields_list_fields_with_type(proxy, type, callback, user_data);
}

void opimd_messages_fields_list_fields_with_type(const char *type,
		void (*callback)(GError *, char **, gpointer),
		gpointer user_data)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_fields(MESSAGES_BUS_PATH);
	opimd_fields_list_fields_with_type(proxy, type, callback, user_data);
}

