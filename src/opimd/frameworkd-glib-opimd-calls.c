/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */


#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-bindings.h>
#include "frameworkd-glib-opimd-calls.h"
#include "frameworkd-glib-opimd-dbus.h"
#include "dbus/calls.h"

DBusGProxy *opimdCallsBus = NULL;

/* === PIM.Calls interface === */

/* --- PIM.Calls.Add --- */
typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opimd_calls_add_data_t;
static void
opimd_calls_add_callback(DBusGProxy *proxy, char *call_path,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_calls_add_data_t *data =
		(opimd_calls_add_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, call_path, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_calls_add(GHashTable *call_data,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata)
{
	dbus_connect_to_opimd_calls();
	opimd_calls_add_data_t *data =
		g_malloc(sizeof(opimd_calls_add_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Calls_add_async(opimdCallsBus,
			call_data,
			opimd_calls_add_callback,
			data);
}

/* --- PIM.Calls.GetSingleEntrySingleField --- */
typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opimd_calls_get_single_entry_single_field_data_t;
static void
opimd_calls_get_single_entry_single_field_callback(DBusGProxy *proxy,
		char *value, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_calls_get_single_entry_single_field_data_t *data =
		(opimd_calls_get_single_entry_single_field_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, value, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_calls_get_single_entry_single_field(GHashTable *query, char *field,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata)
{
	dbus_connect_to_opimd_calls();
	opimd_calls_get_single_entry_single_field_data_t *data =
		g_malloc(sizeof(opimd_calls_get_single_entry_single_field_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Calls_get_single_entry_single_field_async(
			opimdCallsBus, query, field,
			opimd_calls_get_single_entry_single_field_callback,
			data);
}

/* --- Query --- */
typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opimd_calls_query_data_t;
static void
opimd_calls_query_callback(DBusGProxy *proxy, char *query_path,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_calls_query_data_t *data =
		(opimd_calls_query_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, query_path, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_calls_query(GHashTable *query,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata)
{
	dbus_connect_to_opimd_calls();
	opimd_calls_query_data_t *data =
		g_malloc(sizeof(opimd_calls_query_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Calls_query_async(opimdCallsBus, query,
			opimd_calls_query_callback, data);
}

/* --- GetNewMissedCalls --- */
typedef struct {
	void (*callback) (GError *, const int, gpointer);
	gpointer userdata;
} opimd_calls_get_new_missed_calls_t;
static void
opimd_calls_get_new_missed_calls_callback(DBusGProxy *proxy, const int amount,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_calls_get_new_missed_calls_t *data =
		(opimd_calls_get_new_missed_calls_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, amount, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_calls_get_new_missed_calls(
		void (*callback)(GError *, const int, gpointer),
		gpointer userdata)
{
	dbus_connect_to_opimd_calls();
	opimd_calls_get_new_missed_calls_t *data =
		g_malloc(sizeof(opimd_calls_get_new_missed_calls_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Calls_get_new_missed_calls_async(opimdCallsBus,
			opimd_calls_get_new_missed_calls_callback,
			data);
}

/* --- Signal: PIM.Calls.NewCall --- */
void
opimd_calls_new_call_handler(DBusGProxy *proxy,
		char *path,
		gpointer userdata)
{
	(void)proxy;
	void (*callback) (char *) = NULL;

	callback = userdata;

	if (callback != NULL)
		(*callback)(path);
}

/* --- Signal: PIM.Calls.IncomingCall --- */
void
opimd_calls_incoming_call_handler(DBusGProxy *proxy,
		char *path,
		gpointer userdata)
{
	(void)proxy;
	void (*callback) (char *) = NULL;

	callback = userdata;

	if (callback != NULL)
		(*callback)(path);
}

/* --- Signal: PIM.Calls.NewMissedCalls --- */
void
opimd_calls_new_missed_calls_handler(DBusGProxy *proxy,
		const int amount,
		gpointer userdata)
{
	(void)proxy;
	void (*callback) (const int) = NULL;

	callback = userdata;

	if (callback != NULL)
		(*callback)(amount);
}

/* === PIM.Call interface === */

/* --- PIM.Call.GetContent --- */
typedef struct {
	void (*callback)(GError *, GHashTable *, gpointer);
	gpointer userdata;
} opimd_call_get_content_data_t;
static void
opimd_call_get_content_callback(DBusGProxy *proxy, GHashTable *call_data,
		GError *dbus_error,
		gpointer userdata)
{
	(void)proxy;
	opimd_call_get_content_data_t *data =
		(opimd_call_get_content_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, call_data, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_get_content(const char *call_path,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_call(call_path);
	if (!proxy) {
		g_message("call %s does not exist", call_path);
		return;
	}
	opimd_call_get_content_data_t *data =
		g_malloc(sizeof(opimd_call_get_content_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Call_get_content_async(proxy,
			opimd_call_get_content_callback, data);
}

/* --- PIM.Call.GetMultipleFields --- */
typedef struct {
	void (*callback)(GError *, GHashTable *, gpointer);
	gpointer userdata;
} opimd_call_get_multiple_fields_data_t;
static void
opimd_call_get_multiple_fields_callback(DBusGProxy *proxy,
		GHashTable *field_data, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_get_multiple_fields_data_t *data =
		(opimd_call_get_multiple_fields_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, field_data, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_get_multiple_fields(const char *call_path, const char *fieldlist,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_call(call_path);
	if (!proxy) {
		g_message("call %s does not exist", call_path);
		return;
	}
	opimd_call_get_multiple_fields_data_t *data =
		g_malloc(sizeof(opimd_call_get_multiple_fields_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Call_get_multiple_fields_async(proxy, fieldlist,
			opimd_call_get_multiple_fields_callback, data);
}

/* --- PIM.Call.GetUsedBackends --- */
typedef struct {
	void (*callback)(GError *, char **, gpointer);
	gpointer userdata;
} opimd_call_get_used_backends_data_t;
static void
opimd_call_get_used_backends_callback(DBusGProxy *proxy,
		char **backends, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_get_used_backends_data_t *data =
		(opimd_call_get_used_backends_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, backends, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_get_used_backends(const char *call_path,
		void (*callback)(GError *, char **, gpointer),
		gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_call(call_path);
	if (!proxy) {
		g_message("call %s does not exist", call_path);
		return;
	}
	opimd_call_get_used_backends_data_t *data =
		g_malloc(sizeof(opimd_call_get_used_backends_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Call_get_used_backends_async(proxy,
			opimd_call_get_used_backends_callback, data);
}

/* --- PIM.Call.Update --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_call_update_data_t;
static void
opimd_call_update_callback(DBusGProxy *proxy, GError *dbus_error,
		gpointer userdata)
{
	(void)proxy;
	opimd_call_update_data_t *data =
		(opimd_call_update_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_update(const char *call_path, GHashTable *call_data,
		void (*callback)(GError *, gpointer),
		gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_call(call_path);
	if (!proxy) {
		g_message("call %s does not exist", call_path);
		return;
	}
	opimd_call_update_data_t *data =
		g_malloc(sizeof(opimd_call_update_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Call_update_async(proxy, call_data,
			opimd_call_update_callback, data);
}

/* --- PIM.Call.Delete --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_call_delete_data_t;
static void
opimd_call_delete_callback(DBusGProxy *proxy,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_delete_data_t *data =
		(opimd_call_delete_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_delete(const char *call_path,
		void (*callback)(GError *, gpointer),
		gpointer userdata)
{
	DBusGProxy *proxy = dbus_connect_to_opimd_call(call_path);
	opimd_call_delete_data_t *data =
		g_malloc(sizeof(opimd_call_delete_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_Call_delete_async(proxy,
			opimd_call_delete_callback, data);
}


/* === PIM.CallQuery interface === */

/* --- PIM.CallQuery.GetResultCount --- */
typedef struct {
	void (*callback)(GError *, int, gpointer);
	gpointer userdata;
} opimd_call_query_get_result_count_data_t;
static void
opimd_call_query_get_result_count_callback(DBusGProxy *proxy,
		int count, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_get_result_count_data_t *data =
		(opimd_call_query_get_result_count_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, count, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_get_result_count(DBusGProxy *proxy,
		void (*callback)(GError *, int, gpointer),
		gpointer userdata)
{
	opimd_call_query_get_result_count_data_t *data =
		g_malloc(sizeof(opimd_call_query_get_result_count_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_get_result_count_async(proxy,
			opimd_call_query_get_result_count_callback, data);
}

/* --- PIM.CallQuery.Rewind --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_call_query_rewind_data_t;
static void
opimd_call_query_rewind_callback(DBusGProxy *proxy,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_rewind_data_t *data =
		(opimd_call_query_rewind_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_rewind(DBusGProxy *proxy,
		void (*callback)(GError *, gpointer),
		gpointer userdata)
{
	opimd_call_query_rewind_data_t *data =
		g_malloc(sizeof(opimd_call_query_rewind_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_rewind_async(proxy,
			opimd_call_query_rewind_callback, data);
}

/* --- PIM.CallQuery.Skip --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_call_query_skip_data_t;
static void
opimd_call_query_skip_callback(DBusGProxy *proxy,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_skip_data_t *data =
		(opimd_call_query_skip_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_skip(DBusGProxy *proxy, const int count,
		void (*callback)(GError *, gpointer),
		gpointer userdata)
{
	opimd_call_query_skip_data_t *data =
		g_malloc(sizeof(opimd_call_query_skip_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_skip_async(proxy, count,
			opimd_call_query_skip_callback, data);
}

/* --- PIM.CallQuery.GetCallPath --- */
typedef struct {
	void (*callback)(GError *, char *, gpointer);
	gpointer userdata;
} opimd_call_query_get_call_path_data_t;
static void
opimd_call_query_get_call_path_callback(DBusGProxy *proxy,
		char *call_path, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_get_call_path_data_t *data =
		(opimd_call_query_get_call_path_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, call_path, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_get_call_path(DBusGProxy *proxy,
		void (*callback)(GError *, char *, gpointer),
		gpointer userdata)
{
	opimd_call_query_get_call_path_data_t *data =
		g_malloc(sizeof(opimd_call_query_get_call_path_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_get_call_path_async(proxy,
			opimd_call_query_get_call_path_callback, data);
}

/* --- PIM.CallQuery.GetResult --- */
typedef struct {
	void (*callback)(GError *, GHashTable *, gpointer);
	gpointer userdata;
} opimd_call_query_get_result_data_t;
static void
opimd_call_query_get_result_callback(DBusGProxy *proxy, GHashTable *item,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_get_result_data_t *data =
		(opimd_call_query_get_result_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, item, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_get_result(DBusGProxy *proxy,
		void (*callback)(GError *, GHashTable *, gpointer),
		gpointer userdata)
{
	opimd_call_query_get_result_data_t *data =
		g_malloc(sizeof(opimd_call_query_get_result_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_get_result_async(proxy,
			opimd_call_query_get_result_callback, data);
}

/* --- PIM.CallQuery.GetMultipleResults --- */
typedef struct {
	void (*callback)(GError *, GPtrArray *, gpointer);
	gpointer userdata;
} opimd_call_query_get_multiple_results_data_t;
static void
opimd_call_query_get_multiple_results_callback(DBusGProxy *proxy,
		GPtrArray *resultset, GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_get_multiple_results_data_t *data =
		(opimd_call_query_get_multiple_results_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, resultset, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_get_multiple_results(DBusGProxy *proxy, const int count,
		void (*callback)(GError *, GPtrArray *, gpointer),
		gpointer userdata)
{
	opimd_call_query_get_multiple_results_data_t *data =
		g_malloc(sizeof(opimd_call_query_get_multiple_results_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_get_multiple_results_async(proxy, count,
			opimd_call_query_get_multiple_results_callback, data);
}

/* --- PIM.CallQuery.Dispose --- */
typedef struct {
	void (*callback)(GError *, gpointer);
	gpointer userdata;
} opimd_call_query_dispose_data_t;
static void
opimd_call_query_dispose_callback(DBusGProxy *proxy,
		GError *dbus_error, gpointer userdata)
{
	(void)proxy;
	opimd_call_query_dispose_data_t *data =
		(opimd_call_query_dispose_data_t *)userdata;
	GError *error = NULL;

	if (data->callback != NULL) {
		if (dbus_error != NULL)
			error = dbus_handle_errors(dbus_error);
		data->callback(error, data->userdata);
		if (error != NULL)
			g_error_free(error);
	}
	if (dbus_error != NULL)
		g_error_free(dbus_error);

	g_free(data);
}
void
opimd_call_query_dispose(DBusGProxy *proxy,
		void (*callback)(GError *, gpointer),
		gpointer userdata)
{
	opimd_call_query_dispose_data_t *data =
		g_malloc(sizeof(opimd_call_query_dispose_data_t));
	data->callback = callback;
	data->userdata = userdata;
	org_freesmartphone_PIM_CallQuery_dispose_async(proxy,
			opimd_call_query_dispose_callback, data);
}

/* --- Signal: PIM.CallQuery.CallAdded --- */
void
opimd_call_query_call_added_handler(DBusGProxy *proxy,
		char *call_path, gpointer userdata)
{
	(void)proxy;
	void (*callback) (char *) = NULL;

	callback = userdata;

	if (callback != NULL)
		(*callback)(call_path);
}

