/*
 *  Copyright (C) 2009
 *      Authors (alphabetical) :
 *              mrmoku (Klaus Kurzmann, mok@fluxnetz.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; version 2 of the license.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 */

#ifndef FRAMEWORKD_GLIB_OPIMD_FIELDS_H
#define FRAMEWORKD_GLIB_OPIMD_FIELDS_H

#include <glib.h>
#include <dbus/dbus-glib.h>

void opimd_contacts_fields_add(const char *name, const char *type,
		void (*callback)(GError *, gpointer),
		gpointer user_data);
void opimd_messages_fields_add(const char *name, const char *type,
		void (*callback)(GError *, gpointer),
		gpointer user_data);

void opimd_contacts_fields_delete(const char *name,
		void (*callback)(GError *, gpointer),
		gpointer user_data);
void opimd_messages_fields_delete(const char *name,
		void (*callback)(GError *, gpointer),
		gpointer user_data);

void opimd_contacts_fields_get(const char *name,
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data);
void opimd_messages_fields_get(const char *name,
		void (*callback)(GError *, char *, gpointer),
		gpointer user_data);

void opimd_contacts_fields_list(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer user_data);
void opimd_messages_fields_list(void (*callback)(GError *, GHashTable *, gpointer),
		gpointer user_data);

void opimd_contacts_fields_list_fields_with_type(const char *type,
		void (*callback)(GError *, char **fields, gpointer),
		gpointer user_data);
void opimd_messages_fields_list_fields_with_type(const char *type,
		void (*callback)(GError *, char **fields, gpointer),
		gpointer user_data);;
#endif

